                 .686p
                 .mmx
                 .model flat
 .intel_syntax noprefix

 ; ===========================================================================

 ; Segment type: Pure code
 ; Segment permissions: Read/Execute
 _text           segment para public 'CODE' use32
                 assume cs:_text
                 ;org 401000h
                 assume es:nothing, ss:nothing, ds:_data, fs:nothing, gs:nothing

 ; =============== S U B R O U T I N E =======================================

 ; Attributes: fuzzy-sp

 ___mingw_CRTStartup proc near           ; CODE XREF: _mainCRTStartup+10?p
                                         ; _WinMainCRTStartup+10?p

 lpTopLevelExceptionFilter= dword ptr -3Ch
 var_38          = dword ptr -38h
 var_34          = dword ptr -34h
 var_30          = dword ptr -30h
 var_2C          = dword ptr -2Ch
 var_14          = byte ptr -14h
 var_10          = dword ptr -10h

                 push    ebx
                 sub     esp, 38h
                 mov     eax, ds:___dyn_tls_init_callback
                 test    eax, eax
                 jz      short loc_401029
                 mov     [esp+3Ch+var_34], 0
                 mov     [esp+3Ch+var_38], 2
                 mov     [esp+3Ch+lpTopLevelExceptionFilter], 0
                 call    eax ; ___dyn_tls_init@12
                 sub     esp, 0Ch

 loc_401029:                             ; CODE XREF: ___mingw_CRTStartup+B?j
                 mov     [esp+3Ch+lpTopLevelExceptionFilter], offset __gnu_exception_handler@4 ; lpTopLevelExceptionFilter
                 call    _SetUnhandledExceptionFilter@4
                 sub     esp, 4
                 call    ___cpu_features_init
                 call    _fpreset
                 lea     eax, [esp+3Ch+var_10]
                 mov     [esp+3Ch+var_2C], eax
                 mov     eax, __CRT_glob
                 mov     [esp+3Ch+var_38], offset __argv
                 mov     [esp+3Ch+lpTopLevelExceptionFilter], offset __argc
                 mov     [esp+3Ch+var_10], 0
                 mov     [esp+3Ch+var_30], eax
                 lea     eax, [esp+3Ch+var_14]
                 mov     [esp+3Ch+var_34], eax
                 call    ___getmainargs
                 mov     eax, ds:__CRT_fmode
                 test    eax, eax
                 jz      short loc_4010C2
                 mov     ebx, ds:__imp___iob
                 mov     __fmode, eax
                 mov     [esp+3Ch+var_38], eax ; int
                 mov     eax, [ebx+10h]
                 mov     [esp+3Ch+lpTopLevelExceptionFilter], eax ; int
                 call    __setmode
                 mov     eax, ds:__CRT_fmode
                 mov     [esp+3Ch+var_38], eax ; int
                 mov     eax, [ebx+30h]
                 mov     [esp+3Ch+lpTopLevelExceptionFilter], eax ; int
                 call    __setmode
                 mov     eax, ds:__CRT_fmode
                 mov     [esp+3Ch+var_38], eax ; int
                 mov     eax, [ebx+50h]
                 mov     [esp+3Ch+lpTopLevelExceptionFilter], eax ; int
                 call    __setmode

 loc_4010C2:                             ; CODE XREF: ___mingw_CRTStartup+7E?j
                 call    ___p__fmode
                 mov     edx, __fmode
                 mov     [eax], edx
                 call    __pei386_runtime_relocator
                 and     esp, 0FFFFFFF0h
                 call    ___main
                 call    ___p__environ
                 mov     eax, [eax]
                 mov     [esp+3Ch+var_34], eax
                 mov     eax, ds:__argv
                 mov     [esp+3Ch+var_38], eax
                 mov     eax, ds:__argc
                 mov     [esp+3Ch+lpTopLevelExceptionFilter], eax
                 call    _main
                 mov     ebx, eax
                 call    __cexit
                 mov     [esp+3Ch+lpTopLevelExceptionFilter], ebx ; uExitCode
                 call    _ExitProcess@4
                 lea     esi, [esi+0]
 ___mingw_CRTStartup endp ; sp-analysis failed


 ; =============== S U B R O U T I N E =======================================


 ; LONG __stdcall _gnu_exception_handler(struct _EXCEPTION_POINTERS *ExceptionInfo)
 __gnu_exception_handler@4 proc near     ; DATA XREF: ___mingw_CRTStartup:loc_401029?o

 var_2C          = dword ptr -2Ch
 var_28          = dword ptr -28h
 var_10          = dword ptr -10h
 ExceptionInfo   = dword ptr  4

                 push    ebx
                 sub     esp, 28h
                 mov     eax, [esp+2Ch+ExceptionInfo]
                 mov     eax, [eax]
                 mov     eax, [eax]
                 cmp     eax, 0C0000091h
                 ja      short loc_401160
                 cmp     eax, 0C000008Dh
                 jb      short loc_401177

 loc_40112A:                             ; CODE XREF: __gnu_exception_handler@4+65?j
                 mov     ebx, 1

 loc_40112F:                             ; CODE XREF: __gnu_exception_handler@4+A4?j
                 mov     [esp+2Ch+var_28], 0 ; void (__cdecl *)(int)
                 mov     [esp+2Ch+var_2C], 8 ; int
                 call    _signal
                 cmp     eax, 1
                 jz      loc_401240
                 test    eax, eax
                 jnz     loc_4011F4

 loc_401154:                             ; CODE XREF: __gnu_exception_handler@4+63?j
                                         ; __gnu_exception_handler@4+73?j ...
                 xor     eax, eax

 loc_401156:                             ; CODE XREF: __gnu_exception_handler@4+A0?j
                                         ; __gnu_exception_handler@4+DF?j ...
                 add     esp, 28h
                 pop     ebx
                 retn    4
 ; ---------------------------------------------------------------------------
                 align 10h

 loc_401160:                             ; CODE XREF: __gnu_exception_handler@4+11?j
                 cmp     eax, 0C0000094h
                 jz      short loc_4011B2
                 cmp     eax, 0C0000096h
                 jz      short loc_401185
                 cmp     eax, 0C0000093h
                 jnz     short loc_401154
                 jmp     short loc_40112A
 ; ---------------------------------------------------------------------------

 loc_401177:                             ; CODE XREF: __gnu_exception_handler@4+18?j
                 cmp     eax, 0C0000005h
                 jz      short loc_4011C0
                 cmp     eax, 0C000001Dh
                 jnz     short loc_401154

 loc_401185:                             ; CODE XREF: __gnu_exception_handler@4+5C?j
                 mov     [esp+2Ch+var_28], 0 ; void (__cdecl *)(int)
                 mov     [esp+2Ch+var_2C], 4 ; int
                 call    _signal
                 cmp     eax, 1
                 jz      short loc_401207
                 test    eax, eax
                 jz      short loc_401154
                 mov     [esp+2Ch+var_2C], 4
                 call    eax
                 mov     eax, 0FFFFFFFFh
                 jmp     short loc_401156
 ; ---------------------------------------------------------------------------

 loc_4011B2:                             ; CODE XREF: __gnu_exception_handler@4+55?j
                 xor     ebx, ebx
                 jmp     loc_40112F
 ; ---------------------------------------------------------------------------
                 align 10h

 loc_4011C0:                             ; CODE XREF: __gnu_exception_handler@4+6C?j
                 mov     [esp+2Ch+var_28], 0 ; void (__cdecl *)(int)
                 mov     [esp+2Ch+var_2C], 0Bh ; int
                 call    _signal
                 cmp     eax, 1
                 jz      short loc_401223
                 test    eax, eax
                 jz      loc_401154
                 mov     [esp+2Ch+var_2C], 0Bh
                 call    eax
                 mov     eax, 0FFFFFFFFh
                 jmp     loc_401156
 ; ---------------------------------------------------------------------------

 loc_4011F4:                             ; CODE XREF: __gnu_exception_handler@4+3E?j
                 mov     [esp+2Ch+var_2C], 8
                 call    eax
                 mov     eax, 0FFFFFFFFh
                 jmp     loc_401156
 ; ---------------------------------------------------------------------------

 loc_401207:                             ; CODE XREF: __gnu_exception_handler@4+8C?j
                 mov     [esp+2Ch+var_28], 1 ; void (__cdecl *)(int)
                 mov     [esp+2Ch+var_2C], 4 ; int
                 call    _signal
                 or      eax, 0FFFFFFFFh
                 jmp     loc_401156
 ; ---------------------------------------------------------------------------

 loc_401223:                             ; CODE XREF: __gnu_exception_handler@4+C7?j
                 mov     [esp+2Ch+var_28], 1 ; void (__cdecl *)(int)
                 mov     [esp+2Ch+var_2C], 0Bh ; int
                 call    _signal
                 or      eax, 0FFFFFFFFh
                 jmp     loc_401156
 ; ---------------------------------------------------------------------------
                 align 10h

 loc_401240:                             ; CODE XREF: __gnu_exception_handler@4+36?j
                 mov     [esp+2Ch+var_28], 1 ; void (__cdecl *)(int)
                 mov     [esp+2Ch+var_2C], 8 ; int
                 call    _signal
                 test    ebx, ebx
                 mov     eax, 0FFFFFFFFh
                 jz      loc_401156
                 mov     [esp+2Ch+var_10], eax
                 call    _fpreset
                 mov     eax, [esp+2Ch+var_10]
                 jmp     loc_401156
 __gnu_exception_handler@4 endp

 ; ---------------------------------------------------------------------------
                 align 10h

 ; =============== S U B R O U T I N E =======================================


                 public _mainCRTStartup
 _mainCRTStartup proc near

 var_1C          = dword ptr -1Ch

                 sub     esp, 1Ch
                 mov     [esp+1Ch+var_1C], 1
                 call    ds:__imp____set_app_type
                 call    ___mingw_CRTStartup
                 lea     esi, [esi+0]
                 lea     edi, [edi+0]
 _mainCRTStartup endp ; sp-analysis failed


 ; =============== S U B R O U T I N E =======================================


                 public _WinMainCRTStartup
 _WinMainCRTStartup proc near

 var_1C          = dword ptr -1Ch

                 sub     esp, 1Ch
                 mov     [esp+1Ch+var_1C], 2
                 call    ds:__imp____set_app_type
                 call    ___mingw_CRTStartup
                 lea     esi, [esi+0]
                 lea     edi, [edi+0]
 _WinMainCRTStartup endp ; sp-analysis failed


 ; =============== S U B R O U T I N E =======================================


 ; int __cdecl atexit(void (__cdecl *)())
                 public _atexit
 _atexit         proc near               ; CODE XREF: ___gcc_register_frame+50?p
                                         ; ___do_global_ctors+29?p
                 mov     eax, ds:__imp__atexit
                 jmp     eax
 _atexit         endp

 ; ---------------------------------------------------------------------------
                 align 10h

 ; =============== S U B R O U T I N E =======================================


 ; _onexit_t __cdecl _onexit(_onexit_t)
                 public __onexit
 __onexit        proc near
                 mov     eax, ds:__imp___onexit
                 jmp     eax
 __onexit        endp

 ; ---------------------------------------------------------------------------
                 align 10h

 ; =============== S U B R O U T I N E =======================================

 ; Attributes: bp-based frame

                 public ___gcc_register_frame
 ___gcc_register_frame proc near         ; CODE XREF: _register_frame_ctor+4?j
                 push    ebp
                 mov     ebp, esp
                 sub     esp, 18h
                 mov     eax, _data
                 test    eax, eax
                 jz      short loc_401329
                 mov     dword ptr [esp], offset ModuleName ; "libgcj-16.dll"
                 call    _GetModuleHandleA@4
                 sub     esp, 4
                 test    eax, eax
                 mov     edx, 0
                 jz      short loc_40131C
                 mov     dword ptr [esp+4], offset ProcName ; "_Jv_RegisterClasses"
                 mov     [esp], eax      ; hModule
                 call    _GetProcAddress@8
                 sub     esp, 8
                 mov     edx, eax

 loc_40131C:                             ; CODE XREF: ___gcc_register_frame+25?j
                 test    edx, edx
                 jz      short loc_401329
                 mov     dword ptr [esp], offset _data
                 call    edx

 loc_401329:                             ; CODE XREF: ___gcc_register_frame+D?j
                                         ; ___gcc_register_frame+3E?j
                 mov     dword ptr [esp], offset ___gcc_deregister_frame ; void (__cdecl *)()
                 call    _atexit
                 leave
                 retn
 ___gcc_register_frame endp

 ; ---------------------------------------------------------------------------
                 align 10h

 ; =============== S U B R O U T I N E =======================================

 ; Attributes: bp-based frame

 ; void __cdecl __gcc_deregister_frame()
                 public ___gcc_deregister_frame
 ___gcc_deregister_frame proc near       ; DATA XREF: ___gcc_register_frame:loc_401329?o
                 push    ebp
                 mov     ebp, esp
                 pop     ebp
                 retn
 ___gcc_deregister_frame endp

 ; ---------------------------------------------------------------------------
                 align 10h

 ; =============== S U B R O U T I N E =======================================

 ; Attributes: bp-based frame fuzzy-sp

 ; int main()
                 public _main
 _main           proc near               ; CODE XREF: ___mingw_CRTStartup+F8?p
                 push    ebp
                 mov     ebp, esp
                 and     esp, 0FFFFFFF0h
                 sub     esp, 20h
                 call    ___main
                 mov     dword ptr [esp+14h], 0FFFFFFFFh
                 mov     dword ptr [esp+4], offset aR ; "r"
                 mov     dword ptr [esp], offset aContattiTxt ; "contatti.txt"
                 call    _fopen
                 mov     [esp+18h], eax
                 mov     dword ptr [esp+1Ch], 0
                 cmp     dword ptr [esp+18h], 0
                 jnz     short loc_4013C4
                 call    _PrimoAvvio
                 mov     [esp+1Ch], eax
                 cmp     dword ptr [esp+1Ch], 0
                 jnz     short loc_4013B3
                 mov     dword ptr [esp], offset a311mErroreNell ; "\x1B[31;1m \n    Errore nell'apertura d"...
                 call    _printf
                 mov     eax, 0FFFFFFF9h
                 jmp     locret_4015AF
 ; ---------------------------------------------------------------------------

 loc_4013B3:                             ; CODE XREF: _main+4B?j
                 mov     eax, [esp+18h]
                 mov     [esp], eax      ; FILE *
                 call    _fclose
                 jmp     loc_40159E
 ; ---------------------------------------------------------------------------

 loc_4013C4:                             ; CODE XREF: _main+3B?j
                 mov     eax, [esp+18h]
                 mov     [esp], eax      ; f
                 call    _Carica
                 mov     [esp+1Ch], eax
                 cmp     dword ptr [esp+1Ch], 0
                 jnz     short loc_4013F1
                 mov     dword ptr [esp], offset a311mErroreNell ; "\x1B[31;1m \n    Errore nell'apertura d"...
                 call    _printf
                 mov     eax, 0FFFFFFF9h
                 jmp     locret_4015AF
 ; ---------------------------------------------------------------------------

 loc_4013F1:                             ; CODE XREF: _main+89?j
                 mov     eax, [esp+18h]
                 mov     [esp], eax      ; FILE *
                 call    _fclose
                 jmp     loc_40159E
 ; ---------------------------------------------------------------------------

 loc_401402:                             ; CODE XREF: _main+254?j
                 call    _ClearS

 loc_401407:                             ; CODE XREF: _main+153?j
                                         ; _main+160?j
                 mov     dword ptr [esp], offset aInserisci1PerA ; "\n    Inserisci 1 per aggiungere un con"...
                 call    _puts
                 mov     dword ptr [esp], offset aInserisci2PerV ; "    Inserisci 2 per visualizzare la rub"...
                 call    _puts
                 mov     dword ptr [esp], offset aInserisci3PerS ; "    Inserisci 3 per salvare la rubrica"
                 call    _puts
                 mov     dword ptr [esp], offset aInserisci4PerR ; "    Inserisci 4 per ricercare un contat"...
                 call    _puts
                 mov     dword ptr [esp], offset aInserisci5PerE ; "    Inserisci 5 per eliminare un contat"...
                 call    _puts
                 mov     dword ptr [esp], offset aInserisci6PerL ; "    Inserisci 6 per leggere le info sul"...
                 call    _puts
                 mov     dword ptr [esp], offset aInserisci0PerT ; "    Inserisci 0 per terminare\n"
                 call    _puts
                 mov     dword ptr [esp], offset aInserimento ; "     Inserimento: "
                 call    _printf
                 lea     eax, [esp+14h]
                 mov     [esp+4], eax
                 mov     dword ptr [esp], offset aD ; "%d"
                 call    _scanf
                 mov     eax, [esp+14h]
                 test    eax, eax
                 js      short loc_40148C
                 mov     eax, [esp+14h]
                 cmp     eax, 6
                 jle     short loc_40149D

 loc_40148C:                             ; CODE XREF: _main+131?j
                 call    _ClearS
                 mov     dword ptr [esp], offset a311mValoreNonV_0 ; "\x1B[31;1m\n    Valore non valido!!!   "...
                 call    _printf

 loc_40149D:                             ; CODE XREF: _main+13A?j
                 mov     eax, [esp+14h]
                 test    eax, eax
                 js      loc_401407
                 mov     eax, [esp+14h]
                 cmp     eax, 6
                 jg      loc_401407
                 mov     eax, [esp+14h]
                 test    eax, eax
                 jnz     short loc_401500
                 call    _ClearS
                 mov     eax, [esp+1Ch]
                 mov     [esp], eax      ; p
                 call    _Salva
                 cmp     eax, 0FFFFFFF5h
                 jz      short loc_4014EA
                 mov     dword ptr [esp], offset aProgrammaTermi ; "\n    Programma terminato correttamente"...
                 call    _puts
                 mov     eax, 0
                 jmp     locret_4015AF
 ; ---------------------------------------------------------------------------

 loc_4014EA:                             ; CODE XREF: _main+182?j
                 mov     dword ptr [esp], offset a311mImpossibil ; "\x1B[31;1m\n    Impossibile salvare il "...
                 call    _printf
                 mov     eax, 0FFFFFFF5h
                 jmp     locret_4015AF
 ; ---------------------------------------------------------------------------

 loc_401500:                             ; CODE XREF: _main+16C?j
                 mov     eax, [esp+14h]
                 cmp     eax, 1
                 jnz     short loc_40151E
                 mov     eax, [esp+1Ch]
                 mov     [esp], eax      ; p
                 call    _AggiungiContatto
                 mov     [esp+1Ch], eax
                 jmp     loc_40159E
 ; ---------------------------------------------------------------------------

 loc_40151E:                             ; CODE XREF: _main+1B7?j
                 mov     eax, [esp+14h]
                 cmp     eax, 2
                 jnz     short loc_401535
                 mov     eax, [esp+1Ch]
                 mov     [esp], eax      ; p
                 call    _Stampa
                 jmp     short loc_40159E
 ; ---------------------------------------------------------------------------

 loc_401535:                             ; CODE XREF: _main+1D5?j
                 mov     eax, [esp+14h]
                 cmp     eax, 3
                 jnz     short loc_401562
                 mov     eax, [esp+1Ch]
                 mov     [esp], eax      ; p
                 call    _Salva
                 cmp     eax, 0FFFFFFF5h
                 jnz     short loc_40159E
                 mov     dword ptr [esp], offset a311mImpossibil ; "\x1B[31;1m\n    Impossibile salvare il "...
                 call    _printf
                 mov     eax, 0FFFFFFF5h
                 jmp     short locret_4015AF
 ; ---------------------------------------------------------------------------

 loc_401562:                             ; CODE XREF: _main+1EC?j
                 mov     eax, [esp+14h]
                 cmp     eax, 4
                 jnz     short loc_401579
                 mov     eax, [esp+1Ch]
                 mov     [esp], eax      ; p
                 call    _trova
                 jmp     short loc_40159E
 ; ---------------------------------------------------------------------------

 loc_401579:                             ; CODE XREF: _main+219?j
                 mov     eax, [esp+14h]
                 cmp     eax, 5
                 jz      short loc_40159E
                 mov     eax, [esp+14h]
                 cmp     eax, 6
                 jnz     short loc_401592
                 call    _info
                 jmp     short loc_40159E
 ; ---------------------------------------------------------------------------

 loc_401592:                             ; CODE XREF: _main+239?j
                 mov     dword ptr [esp], offset a311mValoreNonV_0 ; "\x1B[31;1m\n    Valore non valido!!!   "...
                 call    _printf

 loc_40159E:                             ; CODE XREF: _main+6F?j
                                         ; _main+AD?j ...
                 mov     eax, [esp+14h]
                 test    eax, eax
                 jnz     loc_401402
                 mov     eax, 0

 locret_4015AF:                          ; CODE XREF: _main+5E?j
                                         ; _main+9C?j ...
                 leave
                 retn
 _main           endp


 ; =============== S U B R O U T I N E =======================================

 ; Attributes: bp-based frame

 ; s_Nodo *PrimoAvvio()
                 public _PrimoAvvio
 _PrimoAvvio     proc near               ; CODE XREF: _main+3D?p

 End             = byte ptr -0F6h
 ausNumero       = byte ptr -0EEh
 ausCognome      = byte ptr -0A8h
 ausNome         = byte ptr -62h
 Fw              = dword ptr -1Ch
 i               = dword ptr -18h
 VariabileDiControllo= dword ptr -14h
 Puntatore       = dword ptr -10h
 Primo           = dword ptr -0Ch

                 push    ebp
                 mov     ebp, esp
                 sub     esp, 108h
                 mov     [ebp+Primo], 0
                 mov     dword ptr [esp+4], offset aW ; "w"
                 mov     dword ptr [esp], offset aContattiTxt ; "contatti.txt"
                 call    _fopen
                 mov     [ebp+Fw], eax
                 mov     eax, ds:dword_4042CA
                 mov     edx, ds:dword_4042CE
                 mov     dword ptr [ebp+End], eax
                 mov     dword ptr [ebp+End+4], edx
                 mov     [ebp+VariabileDiControllo], 0
                 cmp     [ebp+Fw], 0
                 jnz     loc_401983
                 mov     eax, 0
                 jmp     locret_4019A8
 ; ---------------------------------------------------------------------------

 loc_40160A:                             ; CODE XREF: _PrimoAvvio+3D6?j
                 call    _ClearS
                 mov     dword ptr [esp], offset aCreazioneDella ; "\n    Creazione della Rubrica!\n\n    I"...
                 call    _printf
                 mov     dword ptr [esp], offset aPerTerminareLI ; "    per terminare l'inserimento\n\n    "...
                 call    _printf
                 mov     eax, ds:__imp___iob
                 mov     [esp+8], eax    ; FILE *
                 mov     dword ptr [esp+4], 46h ; int
                 lea     eax, [ebp+ausNome]
                 mov     [esp], eax      ; char *
                 call    _fgets
                 mov     [ebp+i], 0
                 jmp     short loc_401679
 ; ---------------------------------------------------------------------------

 loc_40164C:                             ; CODE XREF: _PrimoAvvio+DA?j
                 lea     edx, [ebp+ausNome]
                 mov     eax, [ebp+i]
                 add     eax, edx
                 movzx   eax, byte ptr [eax]
                 cmp     al, 0Ah
                 jz      short loc_40166A
                 lea     edx, [ebp+ausNome]
                 mov     eax, [ebp+i]
                 add     eax, edx
                 movzx   eax, byte ptr [eax]
                 cmp     al, 0Dh
                 jnz     short loc_401675

 loc_40166A:                             ; CODE XREF: _PrimoAvvio+A8?j
                 lea     edx, [ebp+ausNome]
                 mov     eax, [ebp+i]
                 add     eax, edx
                 mov     byte ptr [eax], 0

 loc_401675:                             ; CODE XREF: _PrimoAvvio+B7?j
                 add     [ebp+i], 1

 loc_401679:                             ; CODE XREF: _PrimoAvvio+99?j
                 lea     eax, [ebp+ausNome]
                 mov     [esp], eax      ; char *
                 call    _strlen
                 mov     edx, eax
                 mov     eax, [ebp+i]
                 cmp     edx, eax
                 ja      short loc_40164C
                 lea     eax, [ebp+End]
                 mov     [esp+4], eax    ; char *
                 lea     eax, [ebp+ausNome]
                 mov     [esp], eax      ; char *
                 call    _stricmp
                 test    eax, eax
                 jnz     short loc_4016B2
                 mov     [ebp+VariabileDiControllo], 0FFFFFFFFh
                 jmp     loc_40198D
 ; ---------------------------------------------------------------------------

 loc_4016B2:                             ; CODE XREF: _PrimoAvvio+F3?j
                 mov     dword ptr [esp], offset aCognome ; "     Cognome: "
                 call    _printf
                 mov     eax, ds:__imp___iob
                 mov     [esp+8], eax    ; FILE *
                 mov     dword ptr [esp+4], 46h ; int
                 lea     eax, [ebp+ausCognome]
                 mov     [esp], eax      ; char *
                 call    _fgets
                 mov     [ebp+i], 0
                 jmp     short loc_40171C
 ; ---------------------------------------------------------------------------

 loc_4016E6:                             ; CODE XREF: _PrimoAvvio+180?j
                 lea     edx, [ebp+ausCognome]
                 mov     eax, [ebp+i]
                 add     eax, edx
                 movzx   eax, byte ptr [eax]
                 cmp     al, 0Ah
                 jz      short loc_40170A
                 lea     edx, [ebp+ausCognome]
                 mov     eax, [ebp+i]
                 add     eax, edx
                 movzx   eax, byte ptr [eax]
                 cmp     al, 0Dh
                 jnz     short loc_401718

 loc_40170A:                             ; CODE XREF: _PrimoAvvio+145?j
                 lea     edx, [ebp+ausCognome]
                 mov     eax, [ebp+i]
                 add     eax, edx
                 mov     byte ptr [eax], 0

 loc_401718:                             ; CODE XREF: _PrimoAvvio+157?j
                 add     [ebp+i], 1

 loc_40171C:                             ; CODE XREF: _PrimoAvvio+133?j
                 lea     eax, [ebp+ausCognome]
                 mov     [esp], eax      ; char *
                 call    _strlen
                 mov     edx, eax
                 mov     eax, [ebp+i]
                 cmp     edx, eax
                 ja      short loc_4016E6
                 mov     dword ptr [esp], offset aNumero ; "     Numero: "
                 call    _printf
                 mov     eax, ds:__imp___iob
                 mov     [esp+8], eax    ; FILE *
                 mov     dword ptr [esp+4], 46h ; int
                 lea     eax, [ebp+ausNumero]
                 mov     [esp], eax      ; char *
                 call    _fgets
                 lea     eax, [ebp+ausNumero]
                 mov     [esp], eax      ; char *
                 call    _strlen
                 sub     eax, 1
                 mov     [ebp+eax+ausNumero], 0
                 mov     [ebp+i], 0
                 jmp     short loc_4017B6
 ; ---------------------------------------------------------------------------

 loc_401780:                             ; CODE XREF: _PrimoAvvio+21A?j
                 lea     edx, [ebp+ausNumero]
                 mov     eax, [ebp+i]
                 add     eax, edx
                 movzx   eax, byte ptr [eax]
                 cmp     al, 0Ah
                 jz      short loc_4017A4
                 lea     edx, [ebp+ausNumero]
                 mov     eax, [ebp+i]
                 add     eax, edx
                 movzx   eax, byte ptr [eax]
                 cmp     al, 0Dh
                 jnz     short loc_4017B2

 loc_4017A4:                             ; CODE XREF: _PrimoAvvio+1DF?j
                 lea     edx, [ebp+ausNumero]
                 mov     eax, [ebp+i]
                 add     eax, edx
                 mov     byte ptr [eax], 0

 loc_4017B2:                             ; CODE XREF: _PrimoAvvio+1F1?j
                 add     [ebp+i], 1

 loc_4017B6:                             ; CODE XREF: _PrimoAvvio+1CD?j
                 lea     eax, [ebp+ausNumero]
                 mov     [esp], eax      ; char *
                 call    _strlen
                 mov     edx, eax
                 mov     eax, [ebp+i]
                 cmp     edx, eax
                 ja      short loc_401780
                 lea     eax, [ebp+End]
                 mov     [esp+4], eax    ; char *
                 lea     eax, [ebp+ausNome]
                 mov     [esp], eax      ; char *
                 call    _strcmp
                 test    eax, eax
                 jz      loc_40192B
                 lea     eax, [ebp+ausNome]
                 mov     [esp+8], eax
                 mov     dword ptr [esp+4], offset aS ; "%s,"
                 mov     eax, [ebp+Fw]
                 mov     [esp], eax      ; FILE *
                 call    _fprintf
                 lea     eax, [ebp+ausCognome]
                 mov     [esp+8], eax
                 mov     dword ptr [esp+4], offset aS ; "%s,"
                 mov     eax, [ebp+Fw]
                 mov     [esp], eax      ; FILE *
                 call    _fprintf
                 lea     eax, [ebp+ausNumero]
                 mov     [esp+8], eax
                 mov     dword ptr [esp+4], offset aS_0 ; "%s\n"
                 mov     eax, [ebp+Fw]
                 mov     [esp], eax      ; FILE *
                 call    _fprintf
                 mov     [ebp+VariabileDiControllo], 0
                 cmp     [ebp+Primo], 0
                 jnz     short loc_4018C1
                 mov     dword ptr [esp], 0D8h ; size_t
                 call    _malloc
                 mov     [ebp+Primo], eax
                 mov     eax, [ebp+Primo]
                 lea     edx, [ebp+ausNome]
                 mov     [esp+4], edx    ; char *
                 mov     [esp], eax      ; char *
                 call    _strcpy
                 mov     eax, [ebp+Primo]
                 lea     edx, [eax+46h]
                 lea     eax, [ebp+ausCognome]
                 mov     [esp+4], eax    ; char *
                 mov     [esp], edx      ; char *
                 call    _strcpy
                 mov     eax, [ebp+Primo]
                 lea     edx, [eax+8Ch]
                 lea     eax, [ebp+ausNumero]
                 mov     [esp+4], eax    ; char *
                 mov     [esp], edx      ; char *
                 call    _strcpy
                 mov     dword ptr [esp], 0D8h ; size_t
                 call    _malloc
                 mov     edx, eax
                 mov     eax, [ebp+Primo]
                 mov     [eax+0D4h], edx
                 mov     eax, [ebp+Primo]
                 mov     [ebp+Puntatore], eax
                 jmp     loc_401983
 ; ---------------------------------------------------------------------------

 loc_4018C1:                             ; CODE XREF: _PrimoAvvio+298?j
                 mov     eax, [ebp+Puntatore]
                 mov     eax, [eax+0D4h]
                 mov     [ebp+Puntatore], eax
                 mov     eax, [ebp+Puntatore]
                 lea     edx, [ebp+ausNome]
                 mov     [esp+4], edx    ; char *
                 mov     [esp], eax      ; char *
                 call    _strcpy
                 mov     eax, [ebp+Puntatore]
                 lea     edx, [eax+46h]
                 lea     eax, [ebp+ausCognome]
                 mov     [esp+4], eax    ; char *
                 mov     [esp], edx      ; char *
                 call    _strcpy
                 mov     eax, [ebp+Puntatore]
                 lea     edx, [eax+8Ch]
                 lea     eax, [ebp+ausNumero]
                 mov     [esp+4], eax    ; char *
                 mov     [esp], edx      ; char *
                 call    _strcpy
                 mov     dword ptr [esp], 0D8h ; size_t
                 call    _malloc
                 mov     edx, eax
                 mov     eax, [ebp+Puntatore]
                 mov     [eax+0D4h], edx
                 jmp     short loc_401983
 ; ---------------------------------------------------------------------------

 loc_40192B:                             ; CODE XREF: _PrimoAvvio+233?j
                 lea     eax, [ebp+End]
                 mov     [esp+4], eax    ; char *
                 lea     eax, [ebp+ausNome]
                 mov     [esp], eax      ; char *
                 call    _strcmp
                 test    eax, eax
                 jz      short loc_40197C
                 lea     eax, [ebp+End]
                 mov     [esp+4], eax    ; char *
                 lea     eax, [ebp+ausCognome]
                 mov     [esp], eax      ; char *
                 call    _strcmp
                 test    eax, eax
                 jz      short loc_40197C
                 lea     eax, [ebp+End]
                 mov     [esp+4], eax    ; char *
                 lea     eax, [ebp+ausNumero]
                 mov     [esp], eax      ; char *
                 call    _strcmp
                 test    eax, eax
                 jnz     short loc_401983

 loc_40197C:                             ; CODE XREF: _PrimoAvvio+391?j
                                         ; _PrimoAvvio+3AD?j
                 mov     [ebp+VariabileDiControllo], 0FFFFFFFFh

 loc_401983:                             ; CODE XREF: _PrimoAvvio+49?j
                                         ; _PrimoAvvio+30B?j ...
                 cmp     [ebp+VariabileDiControllo], 0FFFFFFFFh
                 jnz     loc_40160A

 loc_40198D:                             ; CODE XREF: _PrimoAvvio+FC?j
                 mov     eax, [ebp+Puntatore]
                 mov     dword ptr [eax+0D4h], 0
                 mov     eax, [ebp+Fw]
                 mov     [esp], eax      ; FILE *
                 call    _fclose
                 mov     eax, [ebp+Primo]

 locret_4019A8:                          ; CODE XREF: _PrimoAvvio+54?j
                 leave
                 retn
 _PrimoAvvio     endp


 ; =============== S U B R O U T I N E =======================================

 ; Attributes: bp-based frame

 ; void __cdecl Stampa(s_Nodo *p)
                 public _Stampa
 _Stampa         proc near               ; CODE XREF: _main+1DE?p

 i               = dword ptr -0Ch
 p               = dword ptr  8

                 push    ebp
                 mov     ebp, esp
                 push    ebx
                 sub     esp, 34h
                 call    _ClearS
                 mov     [ebp+i], 1
                 jmp     short loc_401A14
 ; ---------------------------------------------------------------------------

 loc_4019BF:                             ; CODE XREF: _Stampa+6E?j
                 mov     eax, [ebp+p]
                 mov     edx, [eax+0D4h]
                 mov     eax, [ebp+p]
                 lea     ebx, [eax+8Ch]
                 mov     eax, [ebp+p]
                 lea     ecx, [eax+46h]
                 mov     eax, [ebp+p]
                 mov     [esp+18h], edx
                 mov     edx, [ebp+p]
                 mov     [esp+14h], edx
                 mov     [esp+10h], ebx
                 mov     [esp+0Ch], ecx
                 mov     [esp+8], eax
                 mov     eax, [ebp+i]
                 mov     [esp+4], eax
                 mov     dword ptr [esp], offset aDSSSThisDNextD ; "\n    %d -> %s, %s, %s, this:%d, next:%"...
                 call    _printf
                 mov     eax, [ebp+p]
                 mov     eax, [eax+0D4h]
                 mov     [ebp+p], eax
                 add     [ebp+i], 1

 loc_401A14:                             ; CODE XREF: _Stampa+13?j
                 cmp     [ebp+p], 0
                 jnz     short loc_4019BF
                 mov     dword ptr [esp], offset aPremiUnTastoQu ; "\n\n    Premi un tasto qualsiasi per co"...
                 call    _puts
                 call    _getchar
                 call    _getchar
                 nop
                 add     esp, 34h
                 pop     ebx
                 pop     ebp
                 retn
 _Stampa         endp


 ; =============== S U B R O U T I N E =======================================

 ; Attributes: bp-based frame

 ; void ClearS()
                 public _ClearS
 _ClearS         proc near               ; CODE XREF: _main:loc_401402?p
                                         ; _main:loc_40148C?p ...
                 push    ebp
                 mov     ebp, esp
                 sub     esp, 18h
                 mov     dword ptr [esp], offset aCls ; "cls"
                 call    _system
                 nop
                 leave
                 retn
 _ClearS         endp


 ; =============== S U B R O U T I N E =======================================

 ; Attributes: bp-based frame

 ; s_Nodo *__cdecl AggiungiContatto(s_Nodo *p)
                 public _AggiungiContatto
 _AggiungiContatto proc near             ; CODE XREF: _main+1C0?p

 ausNumero       = byte ptr -0E2h
 ausCognome      = byte ptr -9Ch
 ausNome         = byte ptr -56h
 first           = dword ptr -10h
 i               = dword ptr -0Ch
 p               = dword ptr  8

                 push    ebp
                 mov     ebp, esp
                 sub     esp, 0F8h
                 mov     eax, [ebp+p]
                 mov     [ebp+first], eax
                 call    _getchar
                 call    _ClearS
                 mov     dword ptr [esp], offset aAggiungiUnCont ; "\n    Aggiungi un contatto\n\n     Nome"...
                 call    _printf
                 mov     eax, ds:__imp___iob
                 mov     [esp+8], eax    ; FILE *
                 mov     dword ptr [esp+4], 46h ; int
                 lea     eax, [ebp+ausNome]
                 mov     [esp], eax      ; char *
                 call    _fgets
                 mov     [ebp+i], 0
                 jmp     short loc_401AC3
 ; ---------------------------------------------------------------------------

 loc_401A96:                             ; CODE XREF: _AggiungiContatto+89?j
                 lea     edx, [ebp+ausNome]
                 mov     eax, [ebp+i]
                 add     eax, edx
                 movzx   eax, byte ptr [eax]
                 cmp     al, 0Ah
                 jz      short loc_401AB4
                 lea     edx, [ebp+ausNome]
                 mov     eax, [ebp+i]
                 add     eax, edx
                 movzx   eax, byte ptr [eax]
                 cmp     al, 0Dh
                 jnz     short loc_401ABF

 loc_401AB4:                             ; CODE XREF: _AggiungiContatto+57?j
                 lea     edx, [ebp+ausNome]
                 mov     eax, [ebp+i]
                 add     eax, edx
                 mov     byte ptr [eax], 0

 loc_401ABF:                             ; CODE XREF: _AggiungiContatto+66?j
                 add     [ebp+i], 1

 loc_401AC3:                             ; CODE XREF: _AggiungiContatto+48?j
                 lea     eax, [ebp+ausNome]
                 mov     [esp], eax      ; char *
                 call    _strlen
                 mov     edx, eax
                 mov     eax, [ebp+i]
                 cmp     edx, eax
                 ja      short loc_401A96
                 mov     dword ptr [esp], offset aCognome ; "     Cognome: "
                 call    _printf
                 mov     eax, ds:__imp___iob
                 mov     [esp+8], eax    ; FILE *
                 mov     dword ptr [esp+4], 46h ; int
                 lea     eax, [ebp+ausCognome]
                 mov     [esp], eax      ; char *
                 call    _fgets
                 mov     [ebp+i], 0
                 jmp     short loc_401B41
 ; ---------------------------------------------------------------------------

 loc_401B0B:                             ; CODE XREF: _AggiungiContatto+10A?j
                 lea     edx, [ebp+ausCognome]
                 mov     eax, [ebp+i]
                 add     eax, edx
                 movzx   eax, byte ptr [eax]
                 cmp     al, 0Ah
                 jz      short loc_401B2F
                 lea     edx, [ebp+ausCognome]
                 mov     eax, [ebp+i]
                 add     eax, edx
                 movzx   eax, byte ptr [eax]
                 cmp     al, 0Dh
                 jnz     short loc_401B3D

 loc_401B2F:                             ; CODE XREF: _AggiungiContatto+CF?j
                 lea     edx, [ebp+ausCognome]
                 mov     eax, [ebp+i]
                 add     eax, edx
                 mov     byte ptr [eax], 0

 loc_401B3D:                             ; CODE XREF: _AggiungiContatto+E1?j
                 add     [ebp+i], 1

 loc_401B41:                             ; CODE XREF: _AggiungiContatto+BD?j
                 lea     eax, [ebp+ausCognome]
                 mov     [esp], eax      ; char *
                 call    _strlen
                 mov     edx, eax
                 mov     eax, [ebp+i]
                 cmp     edx, eax
                 ja      short loc_401B0B
                 mov     dword ptr [esp], offset aNumero ; "     Numero: "
                 call    _printf
                 mov     eax, ds:__imp___iob
                 mov     [esp+8], eax    ; FILE *
                 mov     dword ptr [esp+4], 46h ; int
                 lea     eax, [ebp+ausNumero]
                 mov     [esp], eax      ; char *
                 call    _fgets
                 lea     eax, [ebp+ausNumero]
                 mov     [esp], eax      ; char *
                 call    _strlen
                 sub     eax, 1
                 mov     [ebp+eax+ausNumero], 0
                 mov     [ebp+i], 0
                 jmp     short loc_401BDB
 ; ---------------------------------------------------------------------------

 loc_401BA5:                             ; CODE XREF: _AggiungiContatto+1A4?j
                 lea     edx, [ebp+ausNumero]
                 mov     eax, [ebp+i]
                 add     eax, edx
                 movzx   eax, byte ptr [eax]
                 cmp     al, 0Ah
                 jz      short loc_401BC9
                 lea     edx, [ebp+ausNumero]
                 mov     eax, [ebp+i]
                 add     eax, edx
                 movzx   eax, byte ptr [eax]
                 cmp     al, 0Dh
                 jnz     short loc_401BD7

 loc_401BC9:                             ; CODE XREF: _AggiungiContatto+169?j
                 lea     edx, [ebp+ausNumero]
                 mov     eax, [ebp+i]
                 add     eax, edx
                 mov     byte ptr [eax], 0

 loc_401BD7:                             ; CODE XREF: _AggiungiContatto+17B?j
                 add     [ebp+i], 1

 loc_401BDB:                             ; CODE XREF: _AggiungiContatto+157?j
                 lea     eax, [ebp+ausNumero]
                 mov     [esp], eax      ; char *
                 call    _strlen
                 mov     edx, eax
                 mov     eax, [ebp+i]
                 cmp     edx, eax
                 ja      short loc_401BA5
                 cmp     [ebp+p], 0
                 jnz     short loc_401C15
                 mov     dword ptr [esp], 0D8h ; size_t
                 call    _malloc
                 mov     [ebp+p], eax
                 jmp     short loc_401C45
 ; ---------------------------------------------------------------------------

 loc_401C09:                             ; CODE XREF: _AggiungiContatto+1D4?j
                 mov     eax, [ebp+p]
                 mov     eax, [eax+0D4h]
                 mov     [ebp+p], eax

 loc_401C15:                             ; CODE XREF: _AggiungiContatto+1AA?j
                 mov     eax, [ebp+p]
                 mov     eax, [eax+0D4h]
                 test    eax, eax
                 jnz     short loc_401C09
                 mov     dword ptr [esp], 0D8h ; size_t
                 call    _malloc
                 mov     edx, eax
                 mov     eax, [ebp+p]
                 mov     [eax+0D4h], edx
                 mov     eax, [ebp+p]
                 mov     eax, [eax+0D4h]
                 mov     [ebp+p], eax

 loc_401C45:                             ; CODE XREF: _AggiungiContatto+1BB?j
                 mov     eax, [ebp+p]
                 lea     edx, [ebp+ausNome]
                 mov     [esp+4], edx    ; char *
                 mov     [esp], eax      ; char *
                 call    _strcpy
                 mov     eax, [ebp+p]
                 lea     edx, [eax+46h]
                 lea     eax, [ebp+ausCognome]
                 mov     [esp+4], eax    ; char *
                 mov     [esp], edx      ; char *
                 call    _strcpy
                 mov     eax, [ebp+p]
                 lea     edx, [eax+8Ch]
                 lea     eax, [ebp+ausNumero]
                 mov     [esp+4], eax    ; char *
                 mov     [esp], edx      ; char *
                 call    _strcpy
                 mov     eax, [ebp+p]
                 mov     dword ptr [eax+0D4h], 0
                 cmp     [ebp+first], 0
                 jz      short loc_401CA2
                 mov     eax, [ebp+first]
                 jmp     short locret_401CA5
 ; ---------------------------------------------------------------------------

 loc_401CA2:                             ; CODE XREF: _AggiungiContatto+24F?j
                 mov     eax, [ebp+p]

 locret_401CA5:                          ; CODE XREF: _AggiungiContatto+254?j
                 leave
                 retn
 _AggiungiContatto endp


 ; =============== S U B R O U T I N E =======================================

 ; Attributes: bp-based frame

 ; s_Nodo *__cdecl Carica(FILE *f)
                 public _Carica
 _Carica         proc near               ; CODE XREF: _main+7B?p

 aus             = byte ptr -1C2h
 stringa         = byte ptr -0F0h
 k               = dword ptr -1Ch
 j               = dword ptr -18h
 i               = dword ptr -14h
 Puntatore       = dword ptr -10h
 Primo           = dword ptr -0Ch
 var_8           = byte ptr -8
 f               = dword ptr  8

                 push    ebp
                 mov     ebp, esp
                 push    edi
                 sub     esp, 1D4h
                 mov     [ebp+Primo], 0
                 mov     [ebp+Puntatore], 0
                 mov     dword ptr [ebp+stringa], 0
                 lea     edx, [ebp+stringa+4]
                 mov     eax, 0
                 mov     ecx, 34h
                 mov     edi, edx
                 rep stosd
                 jmp     loc_401F43
 ; ---------------------------------------------------------------------------

 loc_401CE2:                             ; CODE XREF: _Carica+2A7?j
                 mov     [ebp+i], 0
                 jmp     short loc_401D19
 ; ---------------------------------------------------------------------------

 loc_401CEB:                             ; CODE XREF: _Carica+76?j
                 lea     edx, [ebp+aus]
                 mov     eax, [ebp+i]
                 add     eax, edx
                 mov     byte ptr [eax], 0
                 lea     edx, [ebp+aus+46h]
                 mov     eax, [ebp+i]
                 add     eax, edx
                 mov     byte ptr [eax], 0
                 lea     edx, [ebp+aus+8Ch]
                 mov     eax, [ebp+i]
                 add     eax, edx
                 mov     byte ptr [eax], 0
                 add     [ebp+i], 1

 loc_401D19:                             ; CODE XREF: _Carica+42?j
                 cmp     [ebp+i], 45h
                 jle     short loc_401CEB
                 movzx   eax, ds:byte_404357
                 mov     [ebp+stringa], al
                 mov     eax, [ebp+f]
                 mov     [esp+8], eax    ; FILE *
                 mov     dword ptr [esp+4], 0D4h ; int
                 lea     eax, [ebp+stringa]
                 mov     [esp], eax      ; char *
                 call    _fgets
                 mov     [ebp+i], 0
                 jmp     short loc_401D88
 ; ---------------------------------------------------------------------------

 loc_401D52:                             ; CODE XREF: _Carica+F6?j
                 lea     edx, [ebp+stringa]
                 mov     eax, [ebp+i]
                 add     eax, edx
                 movzx   eax, byte ptr [eax]
                 cmp     al, 0Ah
                 jz      short loc_401D76
                 lea     edx, [ebp+stringa]
                 mov     eax, [ebp+i]
                 add     eax, edx
                 movzx   eax, byte ptr [eax]
                 cmp     al, 0Dh
                 jnz     short loc_401D84

 loc_401D76:                             ; CODE XREF: _Carica+BB?j
                 lea     edx, [ebp+stringa]
                 mov     eax, [ebp+i]
                 add     eax, edx
                 mov     byte ptr [eax], 0

 loc_401D84:                             ; CODE XREF: _Carica+CD?j
                 add     [ebp+i], 1

 loc_401D88:                             ; CODE XREF: _Carica+A9?j
                 lea     eax, [ebp+stringa]
                 mov     [esp], eax      ; char *
                 call    _strlen
                 mov     edx, eax
                 mov     eax, [ebp+i]
                 cmp     edx, eax
                 ja      short loc_401D52
                 mov     [ebp+j], 0
                 mov     [ebp+k], 0
                 mov     [ebp+i], 0
                 jmp     short loc_401E2E
 ; ---------------------------------------------------------------------------

 loc_401DB6:                             ; CODE XREF: _Carica+19C?j
                 lea     edx, [ebp+stringa]
                 mov     eax, [ebp+i]
                 add     eax, edx
                 movzx   eax, byte ptr [eax]
                 cmp     al, 2Ch
                 jz      short loc_401E19
                 lea     edx, [ebp+stringa]
                 mov     eax, [ebp+i]
                 add     eax, edx
                 movzx   eax, byte ptr [eax]
                 cmp     al, 0Dh
                 jz      short loc_401E19
                 lea     edx, [ebp+stringa]
                 mov     eax, [ebp+i]
                 add     eax, edx
                 movzx   eax, byte ptr [eax]
                 cmp     al, 0Ah
                 jz      short loc_401E19
                 lea     edx, [ebp+stringa]
                 mov     eax, [ebp+i]
                 add     eax, edx
                 movzx   eax, byte ptr [eax]
                 mov     edx, [ebp+j]
                 imul    edx, 46h
                 lea     edi, [ebp+var_8]
                 lea     ecx, [edi+edx]
                 mov     edx, [ebp+k]
                 add     edx, ecx
                 sub     edx, 1BAh
                 mov     [edx], al
                 add     [ebp+k], 1
                 jmp     short loc_401E24
 ; ---------------------------------------------------------------------------

 loc_401E19:                             ; CODE XREF: _Carica+11F?j
                                         ; _Carica+131?j ...
                 add     [ebp+j], 1
                 mov     [ebp+k], 0

 loc_401E24:                             ; CODE XREF: _Carica+170?j
                 cmp     [ebp+j], 3
                 jz      short loc_401E4B
                 add     [ebp+i], 1

 loc_401E2E:                             ; CODE XREF: _Carica+10D?j
                 lea     eax, [ebp+stringa]
                 mov     [esp], eax      ; char *
                 call    _strlen
                 mov     edx, eax
                 mov     eax, [ebp+i]
                 cmp     edx, eax
                 ja      loc_401DB6
                 jmp     short loc_401E4C
 ; ---------------------------------------------------------------------------

 loc_401E4B:                             ; CODE XREF: _Carica+181?j
                 nop

 loc_401E4C:                             ; CODE XREF: _Carica+1A2?j
                 cmp     [ebp+Primo], 0
                 jnz     short loc_401ED0
                 mov     dword ptr [esp], 0D8h ; size_t
                 call    _malloc
                 mov     [ebp+Primo], eax
                 mov     eax, [ebp+Primo]
                 lea     edx, [ebp+aus]
                 mov     [esp+4], edx    ; char *
                 mov     [esp], eax      ; char *
                 call    _strcpy
                 mov     eax, [ebp+Primo]
                 add     eax, 46h
                 lea     edx, [ebp+aus]
                 add     edx, 46h
                 mov     [esp+4], edx    ; char *
                 mov     [esp], eax      ; char *
                 call    _strcpy
                 mov     eax, [ebp+Primo]
                 add     eax, 8Ch
                 lea     edx, [ebp+aus]
                 add     edx, 8Ch
                 mov     [esp+4], edx    ; char *
                 mov     [esp], eax      ; char *
                 call    _strcpy
                 mov     dword ptr [esp], 0D8h ; size_t
                 call    _malloc
                 mov     edx, eax
                 mov     eax, [ebp+Primo]
                 mov     [eax+0D4h], edx
                 mov     eax, [ebp+Primo]
                 mov     [ebp+Puntatore], eax
                 jmp     short loc_401F43
 ; ---------------------------------------------------------------------------

 loc_401ED0:                             ; CODE XREF: _Carica+1A9?j
                 mov     eax, [ebp+Puntatore]
                 mov     eax, [eax+0D4h]
                 mov     [ebp+Puntatore], eax
                 mov     eax, [ebp+Puntatore]
                 lea     edx, [ebp+aus]
                 mov     [esp+4], edx    ; char *
                 mov     [esp], eax      ; char *
                 call    _strcpy
                 mov     eax, [ebp+Puntatore]
                 add     eax, 46h
                 lea     edx, [ebp+aus]
                 add     edx, 46h
                 mov     [esp+4], edx    ; char *
                 mov     [esp], eax      ; char *
                 call    _strcpy
                 mov     eax, [ebp+Puntatore]
                 add     eax, 8Ch
                 lea     edx, [ebp+aus]
                 add     edx, 8Ch
                 mov     [esp+4], edx    ; char *
                 mov     [esp], eax      ; char *
                 call    _strcpy
                 mov     dword ptr [esp], 0D8h ; size_t
                 call    _malloc
                 mov     edx, eax
                 mov     eax, [ebp+Puntatore]
                 mov     [eax+0D4h], edx

 loc_401F43:                             ; CODE XREF: _Carica+36?j
                                         ; _Carica+227?j
                 mov     eax, [ebp+f]
                 mov     eax, [eax+0Ch]
                 and     eax, 10h
                 test    eax, eax
                 jz      loc_401CE2
                 mov     eax, [ebp+Puntatore]
                 mov     dword ptr [eax+0D4h], 0
                 mov     eax, [ebp+f]
                 mov     [esp], eax      ; FILE *
                 call    _fclose
                 mov     eax, [ebp+Primo]
                 add     esp, 1D4h
                 pop     edi
                 pop     ebp
                 retn
 _Carica         endp


 ; =============== S U B R O U T I N E =======================================

 ; Attributes: bp-based frame

 ; int __cdecl Salva(s_Nodo *p)
                 public _Salva
 _Salva          proc near               ; CODE XREF: _main+17A?p
                                         ; _main+1F5?p

 Fw              = dword ptr -10h
 i               = dword ptr -0Ch
 p               = dword ptr  8

                 push    ebp
                 mov     ebp, esp
                 sub     esp, 38h
                 mov     dword ptr [esp+4], offset aW ; "w"
                 mov     dword ptr [esp], offset aContattiTxt ; "contatti.txt"
                 call    _fopen
                 mov     [ebp+Fw], eax
                 mov     [ebp+i], 0
                 cmp     [ebp+Fw], 0
                 jnz     short loc_402003
                 mov     eax, 0FFFFFFF5h
                 jmp     short locret_402024
 ; ---------------------------------------------------------------------------

 loc_401FA9:                             ; CODE XREF: _Salva+8F?j
                 cmp     [ebp+i], 0
                 jz      short loc_401FC2
                 mov     eax, [ebp+Fw]
                 mov     [esp+4], eax    ; FILE *
                 mov     dword ptr [esp], 0Ah ; int
                 call    _fputc

 loc_401FC2:                             ; CODE XREF: _Salva+35?j
                 add     [ebp+i], 1
                 mov     eax, [ebp+p]
                 lea     ecx, [eax+8Ch]
                 mov     eax, [ebp+p]
                 lea     edx, [eax+46h]
                 mov     eax, [ebp+p]
                 mov     [esp+10h], ecx
                 mov     [esp+0Ch], edx
                 mov     [esp+8], eax
                 mov     dword ptr [esp+4], offset aSSS ; "%s,%s,%s"
                 mov     eax, [ebp+Fw]
                 mov     [esp], eax      ; FILE *
                 call    _fprintf
                 mov     eax, [ebp+p]
                 mov     eax, [eax+0D4h]
                 mov     [ebp+p], eax

 loc_402003:                             ; CODE XREF: _Salva+28?j
                 cmp     [ebp+p], 0
                 jnz     short loc_401FA9
                 mov     eax, [ebp+Fw]
                 mov     [esp], eax      ; FILE *
                 call    _fflush
                 mov     eax, [ebp+Fw]
                 mov     [esp], eax      ; FILE *
                 call    _fclose
                 mov     eax, 0

 locret_402024:                          ; CODE XREF: _Salva+2F?j
                 leave
                 retn
 _Salva          endp


 ; =============== S U B R O U T I N E =======================================

 ; Attributes: bp-based frame

 ; void __cdecl trova(s_Nodo *p)
                 public _trova
 _trova          proc near               ; CODE XREF: _main+222?p

 Parametro       = byte ptr -5Ah
 Scelta          = dword ptr -14h
 trovato         = dword ptr -10h
 i               = dword ptr -0Ch
 p               = dword ptr  8

                 push    ebp
                 mov     ebp, esp
                 sub     esp, 88h
                 mov     [ebp+i], 1
                 mov     [ebp+Scelta], 0
                 mov     [ebp+trovato], 0
                 call    _ClearS

 loc_402049:                             ; CODE XREF: _trova+73?j
                                         ; _trova+7B?j
                 mov     dword ptr [esp], offset aInserisci1PerR ; "\n    Inserisci 1 per ricercare un nome"...
                 call    _puts
                 mov     dword ptr [esp], offset aInserisci3PerR ; "    Inserisci 3 per ricercare un numero"...
                 call    _printf
                 lea     eax, [ebp+Scelta]
                 mov     [esp+4], eax
                 mov     dword ptr [esp], offset aD ; "%d"
                 call    _scanf
                 mov     eax, [ebp+Scelta]
                 test    eax, eax
                 js      short loc_402083
                 mov     eax, [ebp+Scelta]
                 cmp     eax, 3
                 jle     short loc_402094

 loc_402083:                             ; CODE XREF: _trova+53?j
                 call    _ClearS
                 mov     dword ptr [esp], offset a311mValoreNonV ; "\x1B[31;1m    Valore non valido!!!   \n"...
                 call    _printf

 loc_402094:                             ; CODE XREF: _trova+5B?j
                 mov     eax, [ebp+Scelta]
                 test    eax, eax
                 js      short loc_402049
                 mov     eax, [ebp+Scelta]
                 cmp     eax, 3
                 jg      short loc_402049
                 mov     eax, [ebp+Scelta]
                 test    eax, eax
                 jnz     short loc_4020B4
                 call    _ClearS
                 jmp     locret_402291
 ; ---------------------------------------------------------------------------

 loc_4020B4:                             ; CODE XREF: _trova+82?j
                 call    _getchar
                 call    _ClearS
                 mov     dword ptr [esp], offset aInserisciLaStr ; "\n    Inserisci la stringa da cercare\n"...
                 call    _printf
                 mov     eax, ds:__imp___iob
                 mov     [esp+8], eax    ; FILE *
                 mov     dword ptr [esp+4], 46h ; int
                 lea     eax, [ebp+Parametro]
                 mov     [esp], eax      ; char *
                 call    _fgets
                 mov     [ebp+i], 0
                 jmp     short loc_40211C
 ; ---------------------------------------------------------------------------

 loc_4020EF:                             ; CODE XREF: _trova+108?j
                 lea     edx, [ebp+Parametro]
                 mov     eax, [ebp+i]
                 add     eax, edx
                 movzx   eax, byte ptr [eax]
                 cmp     al, 0Ah
                 jz      short loc_40210D
                 lea     edx, [ebp+Parametro]
                 mov     eax, [ebp+i]
                 add     eax, edx
                 movzx   eax, byte ptr [eax]
                 cmp     al, 0Dh
                 jnz     short loc_402118

 loc_40210D:                             ; CODE XREF: _trova+D6?j
                 lea     edx, [ebp+Parametro]
                 mov     eax, [ebp+i]
                 add     eax, edx
                 mov     byte ptr [eax], 0

 loc_402118:                             ; CODE XREF: _trova+E5?j
                 add     [ebp+i], 1

 loc_40211C:                             ; CODE XREF: _trova+C7?j
                 lea     eax, [ebp+Parametro]
                 mov     [esp], eax      ; char *
                 call    _strlen
                 mov     edx, eax
                 mov     eax, [ebp+i]
                 cmp     edx, eax
                 ja      short loc_4020EF
                 call    _ClearS
                 mov     [ebp+i], 1
                 jmp     loc_402263
 ; ---------------------------------------------------------------------------

 loc_402141:                             ; CODE XREF: _trova+241?j
                 mov     eax, [ebp+Scelta]
                 cmp     eax, 1
                 jnz     short loc_40219C
                 mov     eax, [ebp+p]
                 lea     edx, [ebp+Parametro]
                 mov     [esp+4], edx    ; char *
                 mov     [esp], eax      ; char *
                 call    _stricmp
                 test    eax, eax
                 jnz     short loc_40219C
                 mov     eax, [ebp+p]
                 lea     ecx, [eax+8Ch]
                 mov     eax, [ebp+p]
                 lea     edx, [eax+46h]
                 mov     eax, [ebp+p]
                 mov     [esp+10h], ecx
                 mov     [esp+0Ch], edx
                 mov     [esp+8], eax
                 mov     eax, [ebp+i]
                 mov     [esp+4], eax
                 mov     dword ptr [esp], offset a3dSSS ; "\n    %3d -> %s, %s, %s"
                 call    _printf
                 mov     [ebp+trovato], 1
                 jmp     loc_402253
 ; ---------------------------------------------------------------------------

 loc_40219C:                             ; CODE XREF: _trova+121?j
                                         ; _trova+137?j
                 mov     eax, [ebp+Scelta]
                 cmp     eax, 2
                 jnz     short loc_4021F7
                 mov     eax, [ebp+p]
                 lea     edx, [eax+46h]
                 lea     eax, [ebp+Parametro]
                 mov     [esp+4], eax    ; char *
                 mov     [esp], edx      ; char *
                 call    _stricmp
                 test    eax, eax
                 jnz     short loc_4021F7
                 mov     eax, [ebp+p]
                 lea     ecx, [eax+8Ch]
                 mov     eax, [ebp+p]
                 lea     edx, [eax+46h]
                 mov     eax, [ebp+p]
                 mov     [esp+10h], ecx
                 mov     [esp+0Ch], edx
                 mov     [esp+8], eax
                 mov     eax, [ebp+i]
                 mov     [esp+4], eax
                 mov     dword ptr [esp], offset a3dSSS ; "\n    %3d -> %s, %s, %s"
                 call    _printf
                 mov     [ebp+trovato], 1
                 jmp     short loc_402253
 ; ---------------------------------------------------------------------------

 loc_4021F7:                             ; CODE XREF: _trova+17C?j
                                         ; _trova+195?j
                 mov     eax, [ebp+Scelta]
                 cmp     eax, 3
                 jnz     short loc_402253
                 mov     eax, [ebp+p]
                 lea     edx, [eax+8Ch]
                 lea     eax, [ebp+Parametro]
                 mov     [esp+4], eax    ; char *
                 mov     [esp], edx      ; char *
                 call    _stricmp
                 test    eax, eax
                 jnz     short loc_402253
                 mov     eax, [ebp+p]
                 lea     ecx, [eax+8Ch]
                 mov     eax, [ebp+p]
                 lea     edx, [eax+46h]
                 mov     eax, [ebp+p]
                 mov     [esp+10h], ecx
                 mov     [esp+0Ch], edx
                 mov     [esp+8], eax
                 mov     eax, [ebp+i]
                 mov     [esp+4], eax
                 mov     dword ptr [esp], offset a3dSSS ; "\n    %3d -> %s, %s, %s"
                 call    _printf
                 mov     [ebp+trovato], 1

 loc_402253:                             ; CODE XREF: _trova+171?j
                                         ; _trova+1CF?j ...
                 mov     eax, [ebp+p]
                 mov     eax, [eax+0D4h]
                 mov     [ebp+p], eax
                 add     [ebp+i], 1

 loc_402263:                             ; CODE XREF: _trova+116?j
                 cmp     [ebp+p], 0
                 jnz     loc_402141
                 cmp     [ebp+trovato], 1
                 jz      short loc_40227F
                 mov     dword ptr [esp], offset a321mStringaNon ; "\x1B[32;1m\n    Stringa non trovata!   "...
                 call    _printf

 loc_40227F:                             ; CODE XREF: _trova+24B?j
                 mov     dword ptr [esp], offset aPremiUnTastoQu ; "\n\n    Premi un tasto qualsiasi per co"...
                 call    _puts
                 call    _getchar
                 nop

 locret_402291:                          ; CODE XREF: _trova+89?j
                 leave
                 retn
 _trova          endp


 ; =============== S U B R O U T I N E =======================================

 ; Attributes: bp-based frame

 ; void info()
                 public _info
 _info           proc near               ; CODE XREF: _main+23B?p
                 push    ebp
                 mov     ebp, esp
                 sub     esp, 18h
                 call    _ClearS
                 mov     dword ptr [esp], offset a361mAutoreTibe ; "\x1B[36;1m\n    Autore: Tiberio Filippo"...
                 call    _printf
                 mov     dword ptr [esp], offset a361mClasse3ai0 ; "\x1B[36;1m\n    Classe: 3Ai   \x1B[0m"
                 call    _printf
                 mov     dword ptr [esp], offset a361mProgrammaU ; "\x1B[36;1m\n    Programma: \"Una sempli"...
                 call    _printf
                 mov     dword ptr [esp], offset a361mRepository ; "\x1B[36;1m\n    Repository: https://git"...
                 call    _printf
                 mov     dword ptr [esp], offset a341mListaRetur ; "\x1B[34;1m\n    Lista Return:   \x1B[0m"
                 call    _printf
                 mov     dword ptr [esp], offset a321m0TuttoCorr ; "\x1B[32;1m\n        +0 -> tutto corrett"...
                 call    _printf
                 mov     dword ptr [esp], offset a321m7ErroreNel ; "\x1B[32;1m\n        -7 -> errore nell'a"...
                 call    _printf
                 mov     dword ptr [esp], offset a321m11ErroreNe ; "\x1B[32;1m\n        -11-> errore nello "...
                 call    _printf
                 mov     dword ptr [esp], offset a331mPremiUnTas ; "\x1B[33;1m\n\n    Premi un tasto qualsi"...
                 call    _printf
                 call    _getchar
                 call    _getchar
                 nop
                 leave
                 retn
 _info           endp

 ; ---------------------------------------------------------------------------
                 align 10h

 ; =============== S U B R O U T I N E =======================================


                 public ___dyn_tls_dtor@12
 ___dyn_tls_dtor@12 proc near            ; DATA XREF: .CRT:___xl_d?o

 var_1C          = dword ptr -1Ch
 var_18          = dword ptr -18h
 var_14          = dword ptr -14h
 arg_0           = dword ptr  4
 arg_4           = dword ptr  8
 arg_8           = dword ptr  0Ch

                 sub     esp, 1Ch
                 mov     eax, [esp+1Ch+arg_4]
                 test    eax, eax
                 jz      short loc_402340
                 cmp     eax, 3
                 jz      short loc_402340
                 mov     eax, 1
                 add     esp, 1Ch
                 retn    0Ch
 ; ---------------------------------------------------------------------------
                 align 10h

 loc_402340:                             ; CODE XREF: ___dyn_tls_dtor@12+9?j
                                         ; ___dyn_tls_dtor@12+E?j
                 mov     edx, [esp+1Ch+arg_8]
                 mov     [esp+1Ch+var_18], eax
                 mov     eax, [esp+1Ch+arg_0]
                 mov     [esp+1Ch+var_14], edx
                 mov     [esp+1Ch+var_1C], eax
                 call    ___mingw_TLScallback
                 mov     eax, 1
                 add     esp, 1Ch
                 retn    0Ch
 ___dyn_tls_dtor@12 endp

 ; ---------------------------------------------------------------------------
                 align 10h

 ; =============== S U B R O U T I N E =======================================


                 public ___dyn_tls_init@12
 ___dyn_tls_init@12 proc near            ; CODE XREF: ___mingw_CRTStartup+24?p
                                         ; DATA XREF: .rdata:___dyn_tls_init_callback?o ...

 var_1C          = dword ptr -1Ch
 var_18          = dword ptr -18h
 var_14          = dword ptr -14h
 arg_0           = dword ptr  4
 arg_4           = dword ptr  8
 arg_8           = dword ptr  0Ch

                 push    esi
                 push    ebx
                 sub     esp, 14h
                 cmp     ds:__CRT_MT, 2
                 mov     eax, [esp+1Ch+arg_4]
                 jz      short loc_40238C
                 mov     ds:__CRT_MT, 2

 loc_40238C:                             ; CODE XREF: ___dyn_tls_init@12+10?j
                 cmp     eax, 2
                 jz      short loc_4023A3
                 cmp     eax, 1
                 jz      short loc_4023D8

 loc_402396:                             ; CODE XREF: ___dyn_tls_init@12+43?j
                                         ; ___dyn_tls_init@12+84?j
                 add     esp, 14h
                 mov     eax, 1
                 pop     ebx
                 pop     esi
                 retn    0Ch
 ; ---------------------------------------------------------------------------

 loc_4023A3:                             ; CODE XREF: ___dyn_tls_init@12+1F?j
                 mov     esi, offset ___xd_z
                 sub     esi, offset ___xd_z
                 sar     esi, 2
                 test    esi, esi
                 jle     short loc_402396
                 xor     ebx, ebx

 loc_4023B7:                             ; CODE XREF: ___dyn_tls_init@12+59?j
                 mov     eax, ds:___xd_z[ebx*4]
                 test    eax, eax
                 jz      short loc_4023C4
                 call    eax ; ___xd_z

 loc_4023C4:                             ; CODE XREF: ___dyn_tls_init@12+50?j
                 add     ebx, 1
                 cmp     ebx, esi
                 jnz     short loc_4023B7
                 add     esp, 14h
                 mov     eax, 1
                 pop     ebx
                 pop     esi
                 retn    0Ch
 ; ---------------------------------------------------------------------------

 loc_4023D8:                             ; CODE XREF: ___dyn_tls_init@12+24?j
                 mov     eax, [esp+1Ch+arg_8]
                 mov     [esp+1Ch+var_18], 1
                 mov     [esp+1Ch+var_14], eax
                 mov     eax, [esp+1Ch+arg_0]
                 mov     [esp+1Ch+var_1C], eax
                 call    ___mingw_TLScallback
                 jmp     short loc_402396
 ___dyn_tls_init@12 endp

 ; ---------------------------------------------------------------------------
                 align 10h

 ; =============== S U B R O U T I N E =======================================


                 public ___tlregdtor
 ___tlregdtor    proc near
                 xor     eax, eax
                 retn
 ___tlregdtor    endp

 ; ---------------------------------------------------------------------------
                 align 10h

 ; =============== S U B R O U T I N E =======================================


                 public ___cpu_features_init
 ___cpu_features_init proc near          ; CODE XREF: ___mingw_CRTStartup+38?p
                 pushf
                 pushf
                 pop     eax
                 mov     edx, eax
                 xor     eax, 200000h
                 push    eax
                 popf
                 pushf
                 pop     eax
                 popf
                 xor     eax, edx
                 test    eax, 200000h
                 jz      locret_4024D1
                 push    ebx
                 xor     eax, eax
                 cpuid
                 test    eax, eax
                 jz      loc_4024D0
                 mov     eax, 1
                 cpuid
                 test    dh, 1
                 jz      short loc_40244C
                 or      ds:___cpu_features, 1

 loc_40244C:                             ; CODE XREF: ___cpu_features_init+33?j
                 test    dh, 80h
                 jz      short loc_402458
                 or      ds:___cpu_features, 2

 loc_402458:                             ; CODE XREF: ___cpu_features_init+3F?j
                 test    edx, 800000h
                 jz      short loc_402467
                 or      ds:___cpu_features, 4

 loc_402467:                             ; CODE XREF: ___cpu_features_init+4E?j
                 test    edx, 1000000h
                 jz      short loc_402476
                 or      ds:___cpu_features, 8

 loc_402476:                             ; CODE XREF: ___cpu_features_init+5D?j
                 test    edx, 2000000h
                 jz      short loc_402485
                 or      ds:___cpu_features, 10h

 loc_402485:                             ; CODE XREF: ___cpu_features_init+6C?j
                 and     edx, 4000000h
                 jz      short loc_402494
                 or      ds:___cpu_features, 20h

 loc_402494:                             ; CODE XREF: ___cpu_features_init+7B?j
                 test    cl, 1
                 jz      short loc_4024A0
                 or      ds:___cpu_features, 40h

 loc_4024A0:                             ; CODE XREF: ___cpu_features_init+87?j
                 and     ch, 20h
                 jnz     short loc_4024D3

 loc_4024A5:                             ; CODE XREF: ___cpu_features_init+CD?j
                 mov     eax, 80000000h
                 cpuid
                 cmp     eax, 80000000h
                 jbe     short loc_4024D0
                 mov     eax, 80000001h
                 cpuid
                 test    edx, edx
                 js      short loc_4024E0

 loc_4024BE:                             ; CODE XREF: ___cpu_features_init+DA?j
                 and     edx, 40000000h
                 jz      short loc_4024D0
                 or      ds:___cpu_features, 200h

 loc_4024D0:                             ; CODE XREF: ___cpu_features_init+23?j
                                         ; ___cpu_features_init+A1?j ...
                 pop     ebx

 locret_4024D1:                          ; CODE XREF: ___cpu_features_init+16?j
                 rep retn
 ; ---------------------------------------------------------------------------

 loc_4024D3:                             ; CODE XREF: ___cpu_features_init+93?j
                 or      ds:___cpu_features, 80h
                 jmp     short loc_4024A5
 ; ---------------------------------------------------------------------------
                 align 10h

 loc_4024E0:                             ; CODE XREF: ___cpu_features_init+AC?j
                 or      ds:___cpu_features, 100h
                 jmp     short loc_4024BE
 ___cpu_features_init endp

 ; ---------------------------------------------------------------------------
                 align 10h

 ; =============== S U B R O U T I N E =======================================


 ; void __cdecl fpreset()
                 public _fpreset
 _fpreset        proc near               ; CODE XREF: ___mingw_CRTStartup+3D?p
                                         ; __gnu_exception_handler@4+155?p
                 fninit
                 retn
 _fpreset        endp

 ; ---------------------------------------------------------------------------
                 align 10h

 ; =============== S U B R O U T I N E =======================================

 ; Attributes: noreturn

 ; void __cdecl __noreturn __report_error(char *, char)
 ___report_error proc near               ; CODE XREF: ___write_memory_part_0+109?p
                                         ; __pei386_runtime_relocator+CF?p ...

 var_2C          = dword ptr -2Ch
 var_28          = dword ptr -28h
 var_24          = dword ptr -24h
 var_20          = dword ptr -20h
 var_10          = dword ptr -10h
 arg_0           = dword ptr  4
 arg_4           = byte ptr  8

                 push    ebx
                 sub     esp, 28h
                 mov     ebx, ds:__imp___iob
                 lea     eax, [esp+2Ch+arg_4]
                 mov     [esp+2Ch+var_24], 17h ; size_t
                 mov     [esp+2Ch+var_28], 1 ; size_t
                 add     ebx, 40h
                 mov     [esp+2Ch+var_20], ebx ; FILE *
                 mov     [esp+2Ch+var_2C], offset aMingwRuntimeFa ; "Mingw runtime failure:\n"
                 mov     [esp+2Ch+var_10], eax
                 call    _fwrite
                 mov     eax, [esp+2Ch+var_10]
                 mov     [esp+2Ch+var_2C], ebx ; FILE *
                 mov     [esp+2Ch+var_24], eax ; va_list
                 mov     eax, [esp+2Ch+arg_0]
                 mov     [esp+2Ch+var_28], eax ; char *
                 call    _vfprintf
                 call    _abort
 ___report_error endp

 ; ---------------------------------------------------------------------------
                 align 10h

 ; =============== S U B R O U T I N E =======================================


 ; int __fastcall __write_memory_part_0(size_t, void *)
 ___write_memory_part_0 proc near        ; CODE XREF: __pei386_runtime_relocator+102?p
                                         ; __pei386_runtime_relocator+164?p ...

 lpAddress       = dword ptr -5Ch
 lpBuffer        = dword ptr -58h
 dwLength        = dword ptr -54h
 lpflOldProtect  = dword ptr -50h
 var_3E          = byte ptr -3Eh
 var_3D          = byte ptr -3Dh
 flOldProtect    = dword ptr -3Ch
 Buffer          = _MEMORY_BASIC_INFORMATION ptr -38h
 var_10          = dword ptr -10h
 var_C           = dword ptr -0Ch
 var_8           = dword ptr -8
 var_4           = dword ptr -4

                 sub     esp, 5Ch
                 mov     [esp+5Ch+var_10], ebx
                 mov     ebx, eax
                 lea     eax, [esp+5Ch+Buffer]
                 mov     [esp+5Ch+dwLength], 1Ch ; dwLength
                 mov     [esp+5Ch+lpBuffer], eax ; lpBuffer
                 mov     [esp+5Ch+lpAddress], ebx ; lpAddress
                 mov     [esp+5Ch+var_C], esi
                 mov     esi, edx
                 mov     [esp+5Ch+var_8], edi
                 mov     edi, ecx
                 mov     [esp+5Ch+var_4], ebp
                 call    _VirtualQuery@12
                 sub     esp, 0Ch
                 test    eax, eax
                 jz      loc_402656
                 mov     eax, [esp+5Ch+Buffer.Protect]
                 cmp     eax, 4
                 jnz     short loc_4025D0

 loc_4025A5:                             ; CODE XREF: ___write_memory_part_0+73?j
                 mov     [esp+5Ch+dwLength], edi ; size_t
                 mov     [esp+5Ch+lpBuffer], esi ; void *
                 mov     [esp+5Ch+lpAddress], ebx ; void *
                 call    _memcpy

 loc_4025B5:                             ; CODE XREF: ___write_memory_part_0+C5?j
                                         ; ___write_memory_part_0+CC?j ...
                 mov     ebx, [esp+5Ch+var_10]
                 mov     esi, [esp+5Ch+var_C]
                 mov     edi, [esp+5Ch+var_8]
                 mov     ebp, [esp+5Ch+var_4]
                 add     esp, 5Ch
                 retn
 ; ---------------------------------------------------------------------------
                 align 10h

 loc_4025D0:                             ; CODE XREF: ___write_memory_part_0+43?j
                 cmp     eax, 40h
                 jz      short loc_4025A5
                 mov     eax, [esp+5Ch+Buffer.RegionSize]
                 lea     ebp, [esp+5Ch+flOldProtect]
                 mov     [esp+5Ch+lpflOldProtect], ebp ; lpflOldProtect
                 mov     [esp+5Ch+dwLength], 40h ; flNewProtect
                 mov     [esp+5Ch+lpBuffer], eax ; dwSize
                 mov     eax, [esp+5Ch+Buffer.BaseAddress]
                 mov     [esp+5Ch+lpAddress], eax ; lpAddress
                 call    _VirtualProtect@16
                 sub     esp, 10h
                 mov     eax, [esp+5Ch+Buffer.Protect]
                 mov     [esp+5Ch+dwLength], edi ; size_t
                 mov     [esp+5Ch+lpBuffer], esi ; void *
                 mov     [esp+5Ch+lpAddress], ebx ; void *
                 cmp     eax, 40h
                 setnz   [esp+5Ch+var_3E]
                 cmp     eax, 4
                 setnz   [esp+5Ch+var_3D]
                 call    _memcpy
                 cmp     [esp+5Ch+var_3D], 0
                 jz      short loc_4025B5
                 cmp     [esp+5Ch+var_3E], 0
                 jz      short loc_4025B5
                 mov     eax, [esp+5Ch+flOldProtect]
                 mov     [esp+5Ch+lpflOldProtect], ebp ; lpflOldProtect
                 mov     [esp+5Ch+dwLength], eax ; flNewProtect
                 mov     eax, [esp+5Ch+Buffer.RegionSize]
                 mov     [esp+5Ch+lpBuffer], eax ; dwSize
                 mov     eax, [esp+5Ch+Buffer.BaseAddress]
                 mov     [esp+5Ch+lpAddress], eax ; lpAddress
                 call    _VirtualProtect@16
                 sub     esp, 10h
                 jmp     loc_4025B5
 ; ---------------------------------------------------------------------------

 loc_402656:                             ; CODE XREF: ___write_memory_part_0+36?j
                 mov     [esp+5Ch+dwLength], ebx
                 mov     [esp+5Ch+lpBuffer], 1Ch ; char
                 mov     [esp+5Ch+lpAddress], offset aVirtualqueryFa ; "  VirtualQuery failed for %d bytes at a"...
                 call    ___report_error
 ___write_memory_part_0 endp

 ; ---------------------------------------------------------------------------
                 align 10h

 ; =============== S U B R O U T I N E =======================================


                 public __pei386_runtime_relocator
 __pei386_runtime_relocator proc near    ; CODE XREF: ___mingw_CRTStartup+CF?p

 var_2C          = dword ptr -2Ch
 var_28          = byte ptr -28h
 var_14          = dword ptr -14h
 var_10          = dword ptr -10h
 var_C           = dword ptr -0Ch
 var_8           = dword ptr -8
 var_4           = dword ptr -4

                 mov     eax, ds:_was_init_31048
                 test    eax, eax
                 jz      short loc_402680

 locret_402679:                          ; CODE XREF: __pei386_runtime_relocator+27?j
                 retn
 ; ---------------------------------------------------------------------------
                 align 10h

 loc_402680:                             ; CODE XREF: __pei386_runtime_relocator+7?j
                 mov     eax, offset __RUNTIME_PSEUDO_RELOC_LIST_END__
                 sub     eax, offset __RUNTIME_PSEUDO_RELOC_LIST_END__
                 cmp     eax, 7
                 mov     ds:_was_init_31048, 1
                 jle     short locret_402679
                 sub     esp, 2Ch
                 cmp     eax, 0Bh
                 mov     [esp+2Ch+var_C], ebx
                 mov     [esp+2Ch+var_8], esi
                 mov     [esp+2Ch+var_4], edi
                 jle     loc_402790
                 mov     esi, ds:__RUNTIME_PSEUDO_RELOC_LIST_END__
                 test    esi, esi
                 jnz     loc_402744
                 mov     ebx, ds:dword_4047AC
                 test    ebx, ebx
                 jnz     short loc_402744
                 mov     ecx, ds:dword_4047B0
                 mov     ebx, offset unk_4047B4
                 test    ecx, ecx
                 jz      loc_402795
                 mov     ebx, offset __RUNTIME_PSEUDO_RELOC_LIST_END__

 loc_4026E1:                             ; CODE XREF: __pei386_runtime_relocator+130?j
                 mov     eax, [ebx+8]
                 cmp     eax, 1
                 jnz     loc_402834

 loc_4026ED:                             ; CODE XREF: __pei386_runtime_relocator+169?j
                                         ; __pei386_runtime_relocator+19E?j ...
                 add     ebx, 0Ch
                 cmp     ebx, offset __RUNTIME_PSEUDO_RELOC_LIST_END__
                 jnb     loc_40277F
                 movzx   edx, byte ptr [ebx+8]
                 mov     esi, [ebx+4]
                 mov     ecx, [ebx]
                 cmp     edx, 10h
                 lea     eax, [esi+400000h]
                 mov     edi, [ecx+400000h]
                 jz      loc_4027A8
                 cmp     edx, 20h
                 jz      loc_402813
                 cmp     edx, 8
                 jz      loc_4027E0
                 mov     dword ptr [esp+2Ch+var_28], edx ; char
                 mov     [esp+2Ch+var_2C], offset aUnknownPseudoR ; "  Unknown pseudo relocation bit size %d"...
                 mov     [esp+2Ch+var_14], 0
                 call    ___report_error
 ; ---------------------------------------------------------------------------

 loc_402744:                             ; CODE XREF: __pei386_runtime_relocator+49?j
                                         ; __pei386_runtime_relocator+57?j
                 mov     ebx, offset __RUNTIME_PSEUDO_RELOC_LIST_END__

 loc_402749:                             ; CODE XREF: __pei386_runtime_relocator+129?j
                                         ; __pei386_runtime_relocator+136?j
                 cmp     ebx, offset __RUNTIME_PSEUDO_RELOC_LIST_END__
                 jnb     short loc_40277F

 loc_402751:                             ; CODE XREF: __pei386_runtime_relocator+10D?j
                 mov     edx, [ebx+4]
                 mov     ecx, 4          ; size_t
                 lea     eax, [edx+400000h]
                 mov     edx, [edx+400000h]
                 add     edx, [ebx]
                 add     ebx, 8
                 mov     [esp+2Ch+var_10], edx
                 lea     edx, [esp+2Ch+var_10] ; void *
                 call    ___write_memory_part_0
                 cmp     ebx, offset __RUNTIME_PSEUDO_RELOC_LIST_END__
                 jb      short loc_402751

 loc_40277F:                             ; CODE XREF: __pei386_runtime_relocator+86?j
                                         ; __pei386_runtime_relocator+DF?j
                 mov     ebx, [esp+2Ch+var_C]
                 mov     esi, [esp+2Ch+var_8]
                 mov     edi, [esp+2Ch+var_4]
                 add     esp, 2Ch
                 retn
 ; ---------------------------------------------------------------------------
                 align 10h

 loc_402790:                             ; CODE XREF: __pei386_runtime_relocator+3B?j
                 mov     ebx, offset __RUNTIME_PSEUDO_RELOC_LIST_END__

 loc_402795:                             ; CODE XREF: __pei386_runtime_relocator+66?j
                 mov     edx, [ebx]
                 test    edx, edx
                 jnz     short loc_402749
                 mov     eax, [ebx+4]
                 test    eax, eax
                 jz      loc_4026E1
                 jmp     short loc_402749
 ; ---------------------------------------------------------------------------

 loc_4027A8:                             ; CODE XREF: __pei386_runtime_relocator+A4?j
                 movzx   esi, word ptr [esi+400000h]
                 test    si, si
                 movzx   edx, si
                 jns     short loc_4027BD
                 or      edx, 0FFFF0000h

 loc_4027BD:                             ; CODE XREF: __pei386_runtime_relocator+145?j
                 sub     edx, ecx
                 mov     ecx, 2          ; size_t
                 sub     edx, 400000h
                 add     edx, edi
                 mov     [esp+2Ch+var_14], edx
                 lea     edx, [esp+2Ch+var_14] ; void *
                 call    ___write_memory_part_0
                 jmp     loc_4026ED
 ; ---------------------------------------------------------------------------
                 align 10h

 loc_4027E0:                             ; CODE XREF: __pei386_runtime_relocator+B6?j
                 movzx   edx, byte ptr [eax]
                 test    dl, dl
                 movzx   esi, dl
                 jns     short loc_4027F0
                 or      esi, 0FFFFFF00h

 loc_4027F0:                             ; CODE XREF: __pei386_runtime_relocator+178?j
                 mov     edx, esi
                 sub     edx, 400000h
                 sub     edx, ecx
                 mov     ecx, 1          ; size_t
                 add     edx, edi
                 mov     [esp+2Ch+var_14], edx
                 lea     edx, [esp+2Ch+var_14] ; void *
                 call    ___write_memory_part_0
                 jmp     loc_4026ED
 ; ---------------------------------------------------------------------------

 loc_402813:                             ; CODE XREF: __pei386_runtime_relocator+AD?j
                 add     ecx, 400000h
                 sub     edi, ecx
                 mov     ecx, 4          ; size_t
                 add     edi, [eax]
                 lea     edx, [esp+2Ch+var_14] ; void *
                 mov     [esp+2Ch+var_14], edi
                 call    ___write_memory_part_0
                 jmp     loc_4026ED
 ; ---------------------------------------------------------------------------

 loc_402834:                             ; CODE XREF: __pei386_runtime_relocator+77?j
                 mov     dword ptr [esp+2Ch+var_28], eax ; char
                 mov     [esp+2Ch+var_2C], offset aUnknownPseudoR_0 ; "  Unknown pseudo relocation protocol ve"...
                 call    ___report_error
 __pei386_runtime_relocator endp

 ; ---------------------------------------------------------------------------
                 align 10h

 ; =============== S U B R O U T I N E =======================================


 ; void __cdecl __do_global_dtors()
                 public ___do_global_dtors
 ___do_global_dtors proc near            ; DATA XREF: ___do_global_ctors:loc_4028A2?o
                 mov     eax, _p_1761
                 mov     eax, [eax]
                 test    eax, eax
                 jz      short locret_40287A
                 sub     esp, 0Ch
                 xchg    ax, ax

 loc_402860:                             ; CODE XREF: ___do_global_dtors+25?j
                 call    eax
                 mov     eax, _p_1761
                 lea     edx, [eax+4]
                 mov     eax, [eax+4]
                 mov     _p_1761, edx
                 test    eax, eax
                 jnz     short loc_402860
                 add     esp, 0Ch

 locret_40287A:                          ; CODE XREF: ___do_global_dtors+9?j
                 rep retn
 ___do_global_dtors endp

 ; ---------------------------------------------------------------------------
                 align 10h

 ; =============== S U B R O U T I N E =======================================


                 public ___do_global_ctors
 ___do_global_ctors proc near            ; CODE XREF: ___main+1A?j

 var_1C          = dword ptr -1Ch

                 push    ebx
                 sub     esp, 18h
                 mov     ebx, ds:___CTOR_LIST__
                 cmp     ebx, 0FFFFFFFFh
                 jz      short loc_4028B3

 loc_40288F:                             ; CODE XREF: ___do_global_ctors+47?j
                 test    ebx, ebx
                 jz      short loc_4028A2

 loc_402893:                             ; CODE XREF: ___do_global_ctors+20?j
                 call    ds:___CTOR_LIST__[ebx*4]
                 sub     ebx, 1
                 lea     esi, [esi+0]
                 jnz     short loc_402893

 loc_4028A2:                             ; CODE XREF: ___do_global_ctors+11?j
                 mov     [esp+1Ch+var_1C], offset ___do_global_dtors ; void (__cdecl *)()
                 call    _atexit
                 add     esp, 18h
                 pop     ebx
                 retn
 ; ---------------------------------------------------------------------------

 loc_4028B3:                             ; CODE XREF: ___do_global_ctors+D?j
                 xor     ebx, ebx
                 jmp     short loc_4028B9
 ; ---------------------------------------------------------------------------

 loc_4028B7:                             ; CODE XREF: ___do_global_ctors+45?j
                 mov     ebx, eax

 loc_4028B9:                             ; CODE XREF: ___do_global_ctors+35?j
                 lea     eax, [ebx+1]
                 mov     edx, ds:___CTOR_LIST__[eax*4]
                 test    edx, edx
                 jnz     short loc_4028B7
                 jmp     short loc_40288F
 ___do_global_ctors endp

 ; ---------------------------------------------------------------------------
                 align 10h

 ; =============== S U B R O U T I N E =======================================


                 public ___main
 ___main         proc near               ; CODE XREF: ___mingw_CRTStartup+D7?p
                                         ; _main+9?p
                 mov     ecx, ds:_initialized
                 test    ecx, ecx
                 jz      short loc_4028E0
                 rep retn
 ; ---------------------------------------------------------------------------
                 align 10h

 loc_4028E0:                             ; CODE XREF: ___main+8?j
                 mov     ds:_initialized, 1
                 jmp     short ___do_global_ctors
 ___main         endp

 ; ---------------------------------------------------------------------------
                 align 10h

 ; =============== S U B R O U T I N E =======================================


 ___mingwthr_run_key_dtors_part_0 proc near
                                         ; CODE XREF: ___mingw_TLScallback+79?p
                                         ; ___mingw_TLScallback:loc_402B01?p

 lpCriticalSection= dword ptr -1Ch

                 push    esi
                 push    ebx
                 sub     esp, 14h
                 mov     [esp+1Ch+lpCriticalSection], offset ___mingwthr_cs ; lpCriticalSection
                 call    _EnterCriticalSection@4
                 mov     ebx, ds:_key_dtor_list
                 sub     esp, 4
                 test    ebx, ebx
                 jz      short loc_40293B
                 xchg    ax, ax

 loc_402910:                             ; CODE XREF: ___mingwthr_run_key_dtors_part_0+49?j
                 mov     eax, [ebx]
                 mov     [esp+1Ch+lpCriticalSection], eax ; dwTlsIndex
                 call    _TlsGetValue@4  ; TlsGetValue(x)
                 sub     esp, 4
                 mov     esi, eax
                 call    _GetLastError@0
                 test    eax, eax
                 jnz     short loc_402934
                 test    esi, esi
                 jz      short loc_402934
                 mov     eax, [ebx+4]
                 mov     [esp+1Ch+lpCriticalSection], esi
                 call    eax

 loc_402934:                             ; CODE XREF: ___mingwthr_run_key_dtors_part_0+36?j
                                         ; ___mingwthr_run_key_dtors_part_0+3A?j
                 mov     ebx, [ebx+8]
                 test    ebx, ebx
                 jnz     short loc_402910

 loc_40293B:                             ; CODE XREF: ___mingwthr_run_key_dtors_part_0+1C?j
                 mov     [esp+1Ch+lpCriticalSection], offset ___mingwthr_cs ; lpCriticalSection
                 call    _LeaveCriticalSection@4
                 sub     esp, 4
                 add     esp, 14h
                 pop     ebx
                 pop     esi
                 retn
 ___mingwthr_run_key_dtors_part_0 endp


 ; =============== S U B R O U T I N E =======================================


                 public ____w64_mingwthr_add_key_dtor
 ____w64_mingwthr_add_key_dtor proc near

 var_1C          = dword ptr -1Ch
 var_18          = dword ptr -18h
 var_8           = dword ptr -8
 var_4           = dword ptr -4
 arg_0           = dword ptr  4
 arg_4           = dword ptr  8

                 sub     esp, 1Ch
                 mov     eax, ds:___mingwthr_cs_init
                 mov     [esp+1Ch+var_4], esi
                 xor     esi, esi
                 mov     [esp+1Ch+var_8], ebx
                 test    eax, eax
                 jnz     short loc_402974

 loc_402966:                             ; CODE XREF: ____w64_mingwthr_add_key_dtor+8A?j
                 mov     eax, esi
                 mov     ebx, [esp+1Ch+var_8]
                 mov     esi, [esp+1Ch+var_4]
                 add     esp, 1Ch
                 retn
 ; ---------------------------------------------------------------------------

 loc_402974:                             ; CODE XREF: ____w64_mingwthr_add_key_dtor+14?j
                 mov     [esp+1Ch+var_18], 0Ch ; size_t
                 mov     [esp+1Ch+var_1C], 1 ; size_t
                 call    _calloc
                 test    eax, eax
                 mov     ebx, eax
                 jz      short loc_4029D5
                 mov     eax, [esp+1Ch+arg_0]
                 mov     [esp+1Ch+var_1C], offset ___mingwthr_cs ; lpCriticalSection
                 mov     [ebx], eax
                 mov     eax, [esp+1Ch+arg_4]
                 mov     [ebx+4], eax
                 call    _EnterCriticalSection@4
                 mov     eax, ds:_key_dtor_list
                 mov     ds:_key_dtor_list, ebx
                 mov     [ebx+8], eax
                 sub     esp, 4
                 mov     [esp+1Ch+var_1C], offset ___mingwthr_cs ; lpCriticalSection
                 call    _LeaveCriticalSection@4
                 mov     eax, esi
                 sub     esp, 4
                 mov     ebx, [esp+1Ch+var_8]
                 mov     esi, [esp+1Ch+var_4]
                 add     esp, 1Ch
                 retn
 ; ---------------------------------------------------------------------------

 loc_4029D5:                             ; CODE XREF: ____w64_mingwthr_add_key_dtor+3C?j
                 mov     esi, 0FFFFFFFFh
                 jmp     short loc_402966
 ____w64_mingwthr_add_key_dtor endp

 ; ---------------------------------------------------------------------------
                 align 10h

 ; =============== S U B R O U T I N E =======================================


                 public ____w64_mingwthr_remove_key_dtor
 ____w64_mingwthr_remove_key_dtor proc near

 lpCriticalSection= dword ptr -1Ch
 arg_0           = dword ptr  4

                 push    ebx
                 sub     esp, 18h
                 mov     eax, ds:___mingwthr_cs_init
                 mov     ebx, [esp+1Ch+arg_0]
                 test    eax, eax
                 jnz     short loc_4029F8
                 add     esp, 18h
                 xor     eax, eax
                 pop     ebx
                 retn
 ; ---------------------------------------------------------------------------

 loc_4029F8:                             ; CODE XREF: ____w64_mingwthr_remove_key_dtor+F?j
                 mov     [esp+1Ch+lpCriticalSection], offset ___mingwthr_cs ; lpCriticalSection
                 call    _EnterCriticalSection@4
                 mov     edx, ds:_key_dtor_list
                 sub     esp, 4
                 test    edx, edx
                 jz      short loc_402A2F
                 mov     eax, [edx]
                 cmp     eax, ebx
                 jnz     short loc_402A28
                 jmp     short loc_402A64
 ; ---------------------------------------------------------------------------
                 align 10h

 loc_402A20:                             ; CODE XREF: ____w64_mingwthr_remove_key_dtor+4D?j
                 mov     ecx, [eax]
                 cmp     ecx, ebx
                 jz      short loc_402A45
                 mov     edx, eax

 loc_402A28:                             ; CODE XREF: ____w64_mingwthr_remove_key_dtor+35?j
                 mov     eax, [edx+8]
                 test    eax, eax
                 jnz     short loc_402A20

 loc_402A2F:                             ; CODE XREF: ____w64_mingwthr_remove_key_dtor+2F?j
                 mov     [esp+1Ch+lpCriticalSection], offset ___mingwthr_cs ; lpCriticalSection
                 call    _LeaveCriticalSection@4
                 sub     esp, 4

 loc_402A3E:                             ; CODE XREF: ____w64_mingwthr_remove_key_dtor+82?j
                 add     esp, 18h
                 xor     eax, eax
                 pop     ebx
                 retn
 ; ---------------------------------------------------------------------------

 loc_402A45:                             ; CODE XREF: ____w64_mingwthr_remove_key_dtor+44?j
                 mov     ecx, [eax+8]
                 mov     [edx+8], ecx

 loc_402A4B:                             ; CODE XREF: ____w64_mingwthr_remove_key_dtor+8E?j
                 mov     [esp+1Ch+lpCriticalSection], eax ; void *
                 call    _free
                 mov     [esp+1Ch+lpCriticalSection], offset ___mingwthr_cs ; lpCriticalSection
                 call    _LeaveCriticalSection@4
                 sub     esp, 4
                 jmp     short loc_402A3E
 ; ---------------------------------------------------------------------------

 loc_402A64:                             ; CODE XREF: ____w64_mingwthr_remove_key_dtor+37?j
                 mov     eax, [edx+8]
                 mov     ds:_key_dtor_list, eax
                 mov     eax, edx
                 jmp     short loc_402A4B
 ____w64_mingwthr_remove_key_dtor endp


 ; =============== S U B R O U T I N E =======================================


                 public ___mingw_TLScallback
 ___mingw_TLScallback proc near          ; CODE XREF: ___dyn_tls_dtor@12+33?p
                                         ; ___dyn_tls_init@12+7F?p

 lpCriticalSection= dword ptr -1Ch
 arg_4           = dword ptr  8

                 sub     esp, 1Ch
                 mov     eax, [esp+1Ch+arg_4]
                 cmp     eax, 1
                 jz      short loc_402AC0
                 jb      short loc_402A90
                 cmp     eax, 3
                 jz      short loc_402AE0

 loc_402A83:                             ; CODE XREF: ___mingw_TLScallback+31?j
                                         ; ___mingw_TLScallback+4C?j ...
                 mov     eax, 1
                 add     esp, 1Ch
                 retn
 ; ---------------------------------------------------------------------------
                 align 10h

 loc_402A90:                             ; CODE XREF: ___mingw_TLScallback+C?j
                 mov     eax, ds:___mingwthr_cs_init
                 test    eax, eax
                 jnz     short loc_402B01

 loc_402A99:                             ; CODE XREF: ___mingw_TLScallback+96?j
                 mov     eax, ds:___mingwthr_cs_init
                 cmp     eax, 1
                 jnz     short loc_402A83
                 mov     [esp+1Ch+lpCriticalSection], offset ___mingwthr_cs ; lpCriticalSection
                 mov     ds:___mingwthr_cs_init, 0
                 call    _DeleteCriticalSection@4
                 sub     esp, 4
                 jmp     short loc_402A83
 ; ---------------------------------------------------------------------------
                 align 10h

 loc_402AC0:                             ; CODE XREF: ___mingw_TLScallback+A?j
                 mov     eax, ds:___mingwthr_cs_init
                 test    eax, eax
                 jz      short loc_402AF0

 loc_402AC9:                             ; CODE XREF: ___mingw_TLScallback+8F?j
                 mov     ds:___mingwthr_cs_init, 1
                 mov     eax, 1
                 add     esp, 1Ch
                 retn
 ; ---------------------------------------------------------------------------
                 align 10h

 loc_402AE0:                             ; CODE XREF: ___mingw_TLScallback+11?j
                 mov     eax, ds:___mingwthr_cs_init
                 test    eax, eax
                 jz      short loc_402A83
                 call    ___mingwthr_run_key_dtors_part_0
                 jmp     short loc_402A83
 ; ---------------------------------------------------------------------------

 loc_402AF0:                             ; CODE XREF: ___mingw_TLScallback+57?j
                 mov     [esp+1Ch+lpCriticalSection], offset ___mingwthr_cs ; lpCriticalSection
                 call    _InitializeCriticalSection@4
                 sub     esp, 4
                 jmp     short loc_402AC9
 ; ---------------------------------------------------------------------------

 loc_402B01:                             ; CODE XREF: ___mingw_TLScallback+27?j
                 call    ___mingwthr_run_key_dtors_part_0
                 jmp     short loc_402A99
 ___mingw_TLScallback endp

 ; ---------------------------------------------------------------------------
                 align 10h
 ; [00000006 BYTES: COLLAPSED FUNCTION _stricmp. PRESS CTRL-NUMPAD+ TO EXPAND]
                 align 4
 ; [00000006 BYTES: COLLAPSED FUNCTION ___getmainargs. PRESS CTRL-NUMPAD+ TO EXPAND]
                 align 10h
 ; [00000006 BYTES: COLLAPSED FUNCTION __setmode. PRESS CTRL-NUMPAD+ TO EXPAND]
                 align 4
 ; [00000006 BYTES: COLLAPSED FUNCTION ___p__fmode. PRESS CTRL-NUMPAD+ TO EXPAND]
                 align 10h
 ; [00000006 BYTES: COLLAPSED FUNCTION ___p__environ. PRESS CTRL-NUMPAD+ TO EXPAND]
                 align 4
 ; [00000006 BYTES: COLLAPSED FUNCTION __cexit. PRESS CTRL-NUMPAD+ TO EXPAND]
                 align 10h
 ; [00000006 BYTES: COLLAPSED FUNCTION _signal. PRESS CTRL-NUMPAD+ TO EXPAND]
                 align 4
 ; [00000006 BYTES: COLLAPSED FUNCTION _fopen. PRESS CTRL-NUMPAD+ TO EXPAND]
                 align 10h
 ; [00000006 BYTES: COLLAPSED FUNCTION _printf. PRESS CTRL-NUMPAD+ TO EXPAND]
                 align 4
 ; [00000006 BYTES: COLLAPSED FUNCTION _fclose. PRESS CTRL-NUMPAD+ TO EXPAND]
                 align 10h
 ; [00000006 BYTES: COLLAPSED FUNCTION _puts. PRESS CTRL-NUMPAD+ TO EXPAND]
                 align 4
 ; [00000006 BYTES: COLLAPSED FUNCTION _scanf. PRESS CTRL-NUMPAD+ TO EXPAND]
                 align 10h
 ; [00000006 BYTES: COLLAPSED FUNCTION _fgets. PRESS CTRL-NUMPAD+ TO EXPAND]
                 align 4
 ; [00000006 BYTES: COLLAPSED FUNCTION _strlen. PRESS CTRL-NUMPAD+ TO EXPAND]
                 align 10h
 ; [00000006 BYTES: COLLAPSED FUNCTION _strcmp. PRESS CTRL-NUMPAD+ TO EXPAND]
                 align 4
 ; [00000006 BYTES: COLLAPSED FUNCTION _fprintf. PRESS CTRL-NUMPAD+ TO EXPAND]
                 align 10h
 ; [00000006 BYTES: COLLAPSED FUNCTION _malloc. PRESS CTRL-NUMPAD+ TO EXPAND]
                 align 4
 ; [00000006 BYTES: COLLAPSED FUNCTION _strcpy. PRESS CTRL-NUMPAD+ TO EXPAND]
                 align 10h
 ; [00000006 BYTES: COLLAPSED FUNCTION _getchar. PRESS CTRL-NUMPAD+ TO EXPAND]
                 align 4
 ; [00000006 BYTES: COLLAPSED FUNCTION _system. PRESS CTRL-NUMPAD+ TO EXPAND]
                 align 10h
 ; [00000006 BYTES: COLLAPSED FUNCTION _fputc. PRESS CTRL-NUMPAD+ TO EXPAND]
                 align 4
 ; [00000006 BYTES: COLLAPSED FUNCTION _fflush. PRESS CTRL-NUMPAD+ TO EXPAND]
                 align 10h
 ; [00000006 BYTES: COLLAPSED FUNCTION _fwrite. PRESS CTRL-NUMPAD+ TO EXPAND]
                 align 4
 ; [00000006 BYTES: COLLAPSED FUNCTION _vfprintf. PRESS CTRL-NUMPAD+ TO EXPAND]
                 align 10h
 ; [00000006 BYTES: COLLAPSED FUNCTION _abort. PRESS CTRL-NUMPAD+ TO EXPAND]
                 align 4
 ; [00000006 BYTES: COLLAPSED FUNCTION _memcpy. PRESS CTRL-NUMPAD+ TO EXPAND]
                 align 10h
 ; [00000006 BYTES: COLLAPSED FUNCTION _calloc. PRESS CTRL-NUMPAD+ TO EXPAND]
                 align 4
 ; [00000006 BYTES: COLLAPSED FUNCTION _free. PRESS CTRL-NUMPAD+ TO EXPAND]
                 align 10h
 ; [00000006 BYTES: COLLAPSED FUNCTION _SetUnhandledExceptionFilter@4. PRESS CTRL-NUMPAD+ TO EXPAND]
                 align 4
 ; [00000006 BYTES: COLLAPSED FUNCTION _ExitProcess@4. PRESS CTRL-NUMPAD+ TO EXPAND]
                 align 10h
 ; [00000006 BYTES: COLLAPSED FUNCTION _GetModuleHandleA@4. PRESS CTRL-NUMPAD+ TO EXPAND]
                 align 4
 ; [00000006 BYTES: COLLAPSED FUNCTION _GetProcAddress@8. PRESS CTRL-NUMPAD+ TO EXPAND]
                 align 10h
 ; [00000006 BYTES: COLLAPSED FUNCTION _VirtualQuery@12. PRESS CTRL-NUMPAD+ TO EXPAND]
                 align 4
 ; [00000006 BYTES: COLLAPSED FUNCTION _VirtualProtect@16. PRESS CTRL-NUMPAD+ TO EXPAND]
                 align 10h
 ; [00000006 BYTES: COLLAPSED FUNCTION _EnterCriticalSection@4. PRESS CTRL-NUMPAD+ TO EXPAND]
                 align 4
 ; [00000006 BYTES: COLLAPSED FUNCTION TlsGetValue(x). PRESS CTRL-NUMPAD+ TO EXPAND]
                 align 10h
 ; [00000006 BYTES: COLLAPSED FUNCTION _GetLastError@0. PRESS CTRL-NUMPAD+ TO EXPAND]
                 align 4
 ; [00000006 BYTES: COLLAPSED FUNCTION _LeaveCriticalSection@4. PRESS CTRL-NUMPAD+ TO EXPAND]
                 align 10h
 ; [00000006 BYTES: COLLAPSED FUNCTION _DeleteCriticalSection@4. PRESS CTRL-NUMPAD+ TO EXPAND]
                 align 4
 ; [00000006 BYTES: COLLAPSED FUNCTION _InitializeCriticalSection@4. PRESS CTRL-NUMPAD+ TO EXPAND]
                 align 10h

 ; =============== S U B R O U T I N E =======================================

 ; Attributes: bp-based frame

 _register_frame_ctor proc near
                 push    ebp
                 mov     ebp, esp
                 pop     ebp
                 jmp     ___gcc_register_frame
 _register_frame_ctor endp

 ; ---------------------------------------------------------------------------
                 align 10h
                 public ___CTOR_LIST__
 ; func_ptr __CTOR_LIST__[2]
 ___CTOR_LIST__  dd 0FFFFFFFFh, 402C50h  ; DATA XREF: ___do_global_ctors+4?r
                                         ; ___do_global_ctors:loc_402893?r ...
                 dd 0
                 public __DTOR_LIST__
 ; func_ptr _DTOR_LIST__[2]
 __DTOR_LIST__   dd 0FFFFFFFFh, 0
                 align 200h
                 dd 80h dup(?)
 _text           ends

 ; Section 2. (virtual address 00003000)
 ; Virtual size                  : 00000010 (     16.)
 ; Section size in file          : 00000200 (    512.)
 ; Offset to raw data for section: 00002200
 ; Flags C0300040: Data Readable Writable
 ; Alignment     : 4 bytes
 ; ===========================================================================

 ; Segment type: Pure data
 ; Segment permissions: Read/Write
 _data           segment dword public 'DATA' use32
                 assume cs:_data
                 ;org 403000h
                 public __CRT_glob
 __CRT_glob      dd 0FFFFFFFFh           ; DATA XREF: ___mingw_CRTStartup+4A?r
                 public __fmode
 ; int _fmode
 __fmode         dd 4000h                ; DATA XREF: ___mingw_CRTStartup+86?w
                                         ; ___mingw_CRTStartup+C7?r
 _p_1761         dd 402C70h              ; DATA XREF: ___do_global_dtors?r
                                         ; ___do_global_dtors+12?r ...
 _data           dd 0                    ; DATA XREF: ___gcc_register_frame+6?r
                                         ; ___gcc_register_frame+40?o
                 public __data_end__
 __data_end__    db    0