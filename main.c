/**
    Autore: Tiberio Filippo
    Classe: 3Ai
    Programma: "Una semplice Rubrica Telefonica in C sfruttando le liste e file"
    Repository: https://gitlab.com/FilippoTi/rubricac

    Versione attuale: 2.1

    Versione 1
        - 1.0: Costruzione del programma base
        - 1.1: Controllo degli errori
        - 1.3: Implementazione funzioni -> elimina e modifica
    Versione 2
        - 2.0: Riscrittura codice di output per inserire grafica e colori
        - 2.1: Ricontrollo del codice e pulizia parti inutili

    Lista Return:
        +0 -> tutto corretto!
        -7 -> errore nell'aprire il file
        -11-> errore nello scrivere il file
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int NumeroDiContatti = 0;
typedef struct nodo //Struttura dati: Lista singolarmente linkata
{
    char Nome[70];
    char Cognome[70]; //Dati
    char Numero[70];

    struct nodo *next; //Puntatore a next
} s_Nodo;

//Prototipi di funzione raggruppati per tipo di ritorno e tipo di parametro
///Puntatore
s_Nodo *AggiungiContatto(s_Nodo *p);
s_Nodo *elimina(s_Nodo *p);
s_Nodo *Carica(FILE *f);
s_Nodo *PrimoAvvio();

///Void
void Stampa(s_Nodo *p);
void trova(s_Nodo *p);
void modifica(s_Nodo *p);
void ClearS();
void info();

///Int
int Salva(s_Nodo *p);

/// /////////////////////////////////////////////////////////////////////////////////
int main() //Main f.
{
    int Scelta = -1; //Variabile di scelta
    FILE *Fr; //Puntatore a file

    Fr = fopen("contatti.txt", "r"); //Apro il file e creo la lista
    s_Nodo *Lista = NULL;

    if(Fr == NULL) //Se il file non esiste o ha problemi lo creo
    {
        Lista = PrimoAvvio(); //Creo lista e file;
        if(Lista == NULL) //Se la lista � nulla termino con un messaggio d'errore
        {
            printf("\033[31;1m \n    Elenco dati vuoto! \n\033[0m"); //Colori bash
            return -7;
        }
        fclose(Fr); //Chiudo il file
    }
    else //Se il file si apre correttamente lo carico nella mia struttura dati
    {
        Lista = Carica(Fr);
        if(Lista == NULL)//Se ho errori termino con un  messaggio all'utente
        {
            printf("\033[31;1m \n    Elenco dati vuoto! \n\033[0m");
            return -7;
        }
        fclose(Fr); //Chiudo il file
    }

    while(Scelta != 0) //Ciclo while per le scelte
    {
        ClearS(); //clearScreen

        do//chiedo all'utente il valore
        {
            printf("\n\n    Inserisci 1 per aggiungere un contatto\n");
            printf("    Inserisci 2 per visualizzare la rubrica\n");
            printf("    Inserisci 3 per salvare la rubrica\n");
            printf("    Inserisci 4 per ricercare un contatto\n");
            printf("    Inserisci 5 per eliminare un contatto\n");
            printf("    Inserisci 6 per modificare un contatto\n");
            printf("    Inserisci 7 per leggere le info sul programma\n");
            printf("    Inserisci 0 per terminare\n\n");
            printf("\033[32;1m");
            printf("    Inserimento: ");
            printf("\033[0m");
            scanf("%d", &Scelta);
            if(Scelta < 0 || Scelta > 7)
            {
                ClearS();
                printf("\033[31;1m\n    Valore non valido!!!   \n\033[0m");
            }
        }
        while(Scelta < 0 || Scelta > 7);


        if(Scelta == 0) //Alla chiusura: -Pulisco lo schermo e salvo la structDati -Restituisco un messaggios
        {
            ClearS();
            if(Salva(Lista) != -11) //Controllo e termino
            {
                printf("\033[32;1m\n    Programma terminato correttamente!\n\033[0m");
                return 0;
            }
            else
            {
                printf("\033[31;1m\n    Impossibile salvare il file!!   \n\033[0m");
                return -11;
            }
        }
        else if(Scelta == 1) //Aggiungo un contatto
        {
            Lista = AggiungiContatto(Lista);
        }
        else if(Scelta == 2) //Stampo la lista
        {
            Stampa(Lista);
        }
        else if(Scelta == 3) //Salvo la lista
        {
            if(Salva(Lista) == -11) //Controllo e in caso d'errore termino
            {
                printf("\033[31;1m\n    Impossibile salvare il file!!   \n\033[0m");
                return -11;
            }
        }
        else if(Scelta == 4) //Trovo un contatto
        {
            trova(Lista);
        }
        else if(Scelta == 5) //Elimino un contatto
        {
            Lista = elimina(Lista);
        }
        else if(Scelta == 6) //Modifico un contatto
        {
            modifica(Lista);
        }
        else if(Scelta == 7) //Info del programma
        {
            info();
        }
        else
        {
            printf("\033[31;1m\n    Valore non valido!!!   \n\033[0m");
        }
    }

    return 0;
}
/// /////////////////////////////////////////////////////////////////////////////////
s_Nodo* PrimoAvvio()
{
    s_Nodo *Primo = NULL, *Puntatore;
    NumeroDiContatti = 0;

    FILE *Fw; //Gestico il file e lo apro in scrittura
    Fw = fopen("contatti.txt", "w");

    char ausNome[70];
    char ausCognome[70]; //3 stringhe ausiliarie
    char ausNumero[70];

    char End[8] = "termina\0"; //Paragone per il stricmp

    int VariabileDiControllo = 0; //Controlli e indici
    int i;

    if(Fw==NULL) //controllo il file
    {
        return NULL;
    }
    else
    {
        while(VariabileDiControllo != -1)//Finche non termino
        {
            ClearS();
            printf("\n    Creazione della Rubrica!\n\n    Inserisci \"termina\" nel nome\n ");

            printf("   per terminare l'inserimento\n\n\n\033[32;1m    Nome: \033[0m"); //Chiedo il nome e lo pulisco
            fgets(ausNome, sizeof(ausNome), stdin);
            for(i = 0; i< strlen(ausNome); i++)
            {
                if(ausNome[i] == '\n' || ausNome[i] == '\r')
                {
                    ausNome[i] = 0;
                }
            }

            if(stricmp(ausNome, End)==0) //Se contiene "termina"termino
            {
                VariabileDiControllo = -1;
                break;
            }

            printf("\033[32;1m    Cognome: \033[0m"); //Chiedo il cognome e lo pulisco
            fgets(ausCognome, sizeof(ausCognome), stdin);
            for(i = 0; i< strlen(ausCognome); i++)
            {
                if(ausCognome[i] == '\n' || ausCognome[i] == '\r')
                {
                    ausCognome[i] = 0;
                }
            }

            printf("\033[32;1m    Numero: \033[0m"); //Chiedo il numero e lo pulisco
            fgets(ausNumero, sizeof(ausNumero), stdin);
            ausNumero[strlen(ausNumero)-1] = 0;
            for(i = 0; i< strlen(ausNumero); i++)
            {
                if(ausNumero[i] == '\n' || ausNumero[i] == '\r')
                {
                    ausNumero[i] = 0;
                }
            }

            if(strcmp(ausNome, End)!=0) //Controllo che non sia "termina" e stampo nel file
            {
                fprintf(Fw,"%s,",ausNome);
                fprintf(Fw,"%s,",ausCognome);
                fprintf(Fw,"%s\n",ausNumero);
                VariabileDiControllo = 0; //resetto per sicurezza il controllo

                if(Primo  == NULL) //Se � il primo nodo lo creo e copio le stringhe
                {
                    Primo = (s_Nodo *)malloc(sizeof(s_Nodo ));

                    strcpy(Primo ->Nome, ausNome);
                    strcpy(Primo ->Cognome, ausCognome);
                    strcpy(Primo ->Numero, ausNumero);

                    Primo->next = (s_Nodo *)malloc(sizeof(s_Nodo)); //Creo il next e mi prendo l'indirizzo
                    Puntatore = Primo;
                }
                else //Altrimento mi sposto nel next e copio le stringhe
                {
                    Puntatore = Puntatore->next;
                    strcpy(Puntatore ->Nome, ausNome);
                    strcpy(Puntatore ->Cognome, ausCognome);
                    strcpy(Puntatore ->Numero, ausNumero);
                    Puntatore->next = (s_Nodo *)malloc(sizeof(s_Nodo)); //creo il next
                }
            }
            else if(strcmp(ausNome, End)==0 || strcmp(ausCognome, End)==0 || strcmp(ausNumero, End)==0) //Se "Termina" esco
            {
                VariabileDiControllo = -1;
            }
            NumeroDiContatti++; //incremento il numero di contatti
        }

        if(Primo == NULL)
        {
            return NULL;
        }

        Puntatore->next = NULL; //Metto il tappo alla lista
        fclose(Fw); //Chiudo il file

        return Primo; //Ritorno l'indirizzo del primo nodo
    }
}
/// /////////////////////////////////////////////////////////////////////////////////
void Stampa(s_Nodo *p)
{
    ClearS(); //CS

    int i = 1;//Indice

    printf("\033[32;1m\n      Num                       Nome                    Cognome                     Numero  \n\033[0m"); // Stampa indicazioni
    while(p!=NULL) //Stampo mentre scorro la lista
    {
        printf("\n    %3d -> %25s  %25s  %25s", i, p->Nome,p->Cognome,p->Numero); //Considero nomi, cognomi e numeri di lunghezza massima 30 dirante la stampa, in caso di eccesso la stampa viene spostata
        printf("\033[32;1m\n     -------------------------------------------------------------------------------------\033[0m"); //Separatore
        p = p->next; //Scorro
        i++;
    }
    printf("\033[31;1m\n\n    Premi un tasto qualsiasi per continuare!\n\033[0m");
    getchar();
    getchar();
}
/// /////////////////////////////////////////////////////////////////////////////////
void ClearS()
{
#if linux
    system("clear");
#elif _WIN32
    system("cls");
#endif // _WIN32
}
/// /////////////////////////////////////////////////////////////////////////////////
s_Nodo* AggiungiContatto(s_Nodo *p)
{
    s_Nodo* first = p;
    char ausNome[70];
    char ausCognome[70]; //3 stringhe ausiliarie
    char ausNumero[70];
    int i;

    getchar();//Pulizia e reset
    ClearS();

    printf("\n    Aggiungi contatto numero: %d\n\n\033[32;1m    Nome: \033[0m", NumeroDiContatti+1); //Chiedo il nome e lo pulisco da \n\r
    fgets(ausNome, sizeof(ausNome), stdin);
    for(i = 0; i< strlen(ausNome); i++)
    {
        if(ausNome[i] == '\n' || ausNome[i] == '\r')
        {
            ausNome[i] = 0;
        }
    }

    printf("\033[32;1m    Cognome: \033[0m"); //Chiedo il cognome e lo pulisco da \n\r
    fgets(ausCognome, sizeof(ausCognome), stdin);
    for(i = 0; i< strlen(ausCognome); i++)
    {
        if(ausCognome[i] == '\n' || ausCognome[i] == '\r')
        {
            ausCognome[i] = 0;
        }
    }

    printf("\033[32;1m    Numero: \033[0m"); //Chiedo il numero e lo pulisco da \n\r
    fgets(ausNumero, sizeof(ausNumero), stdin);
    ausNumero[strlen(ausNumero)-1] = 0;
    for(i = 0; i< strlen(ausNumero); i++)
    {
        if(ausNumero[i] == '\n' || ausNumero[i] == '\r')
        {
            ausNumero[i] = 0;
        }
    }

    //Riempio la struttura dati (LISTA
    if(p == NULL) //SE vuota creo il primo nodo
    {
        p  = (s_Nodo *)malloc(sizeof(s_Nodo));
    }
    else //Altrimenti mi sposto a next e creo un altro nodo
    {
        while(p ->next != NULL)
        {
            p = p->next;
        }
        p->next = (s_Nodo *)malloc(sizeof(s_Nodo));
        p = p->next;
    }
    //Copio nella struttura le stringhe
    strcpy(p ->Nome, ausNome);
    strcpy(p ->Cognome, ausCognome);
    strcpy(p ->Numero, ausNumero);
    //Metto a null il next
    p->next = NULL;

    NumeroDiContatti++; //Aumento il numero di contatti

    if(first != NULL) //ritorno first se � il primo altrimenti p
    {
        return first;
    }
    else
    {
        return p;
    }

}
/// /////////////////////////////////////////////////////////////////////////////////
s_Nodo* Carica(FILE *f)
{
    s_Nodo  *Primo = NULL, *Puntatore = NULL; //Puntatori per il caricamento

    //Variabili
    char stringa[212] = "";
    char aus[3][70]; //Matrice per la deserializzazione della stringa
    int i, j, k;

    NumeroDiContatti = 0;//Azzero il numero di contatti

    while(feof(f)==0)//Finche posso leggere
    {
        for(i = 0; i<70; i++) //Imposto a 0 la matrice
        {
            aus[0][i] = '\0';
            aus[1][i] = '\0';
            aus[2][i] = '\0';
        }
        strcpy(stringa, ""); //Azzero la stringa

        fgets(stringa, sizeof(stringa), f); //Leggo dal file

        for(i = 0; i<strlen(stringa); i++) //Pulisco la stringa
        {
            if(stringa[i] == '\n' || stringa[i] == '\r')
            {
                stringa[i] = '\0';
            }
        }

        j = 0;
        k = 0;
        for(i = 0; i< strlen(stringa); i++) //Deserializzazione
        {
            if(stringa[i]!=',' && stringa[i]!='\r'&& stringa[i]!='\n') //Finche non trovo la virgola o un terminatore
            {
                aus[j][k]=stringa[i]; //Creo aggiungo alla stringa e sposto l'indice
                k++;
            }
            else
            {
                j++; //Altrimenti cambio stringa e azzero l'indice di riga
                k = 0;
            }
            if(j == 3) //Se ho pi� di 3 stringhe esco
            {
                break;
            }
        }

        if(Primo == NULL) //Se � il primo nodo, lo creo copio le stringhe e preparo il next
        {
            Primo = (s_Nodo *)malloc(sizeof(s_Nodo));
            strcpy(Primo->Nome, aus[0]);
            strcpy(Primo->Cognome, aus[1]);
            strcpy(Primo->Numero, aus[2]);
            Primo->next = (s_Nodo *)malloc(sizeof(s_Nodo));
            Puntatore = Primo;
        }
        else //Altrimento mi sposto a next copio e preparo il prossimo next
        {
            Puntatore = Puntatore->next;
            strcpy(Puntatore->Nome, aus[0]);
            strcpy(Puntatore->Cognome, aus[1]);
            strcpy(Puntatore->Numero, aus[2]);
            Puntatore->next = (s_Nodo *)malloc(sizeof(s_Nodo));
        }
        NumeroDiContatti++; //Aumento il numero di contatti
    }
    Puntatore ->next = NULL; //Metto a NULL l'ultimo next
    fclose(f); //Chiudo il file
    return Primo; //Returno il primo nodo
}
/// /////////////////////////////////////////////////////////////////////////////////
int Salva(s_Nodo *p)
{
    FILE *Fw; //Creo il file e lo apro in scrittura
    Fw = fopen("contatti.txt", "w");
    int i = 0;

    if(Fw == NULL) //Se non riesco ritorno -11
    {
        return -11; //LR
    }
    else //Altrimento
    {
        while(p!=NULL) //Passo la lista fino al null
        {
            if(i!=0)
            {
                fprintf(Fw,"\n"); //print di /n solo da secondo nodo in poi
            }

            i++; //Incremento i e stampo i dati
            fprintf(Fw,"%s,%s,%s", p->Nome,p->Cognome,p->Numero);
            p = p->next;//Scorro la lista
        }

        fflush(Fw); //Flush, chiudo e ritorno
        fclose(Fw);
        return 0;
    }
}
/// /////////////////////////////////////////////////////////////////////////////////
void trova(s_Nodo *p)
{
    //Variabili
    int i = 1;
    int Scelta = 0;
    char Parametro[70];
    int trovato = 0;

    ClearS(); //Cs
    do //Chiedo in che campo ricercare
    {
        printf("\n    Inserisci 1 per ricercare un nome\n    Inserisci 2 per ricercare un cognome\n");
        printf("    Inserisci 3 per ricercare un numero\n    Inserisci 0 per uscire\n\n\033[32;1m    Inserimento: \033[0m");
        scanf("%d", &Scelta);
        if(Scelta<0 || Scelta> 3)
        {
            ClearS();
            printf("\033[31;1m    Valore non valido!!!   \n\033[0m");
        }
    }
    while(Scelta<0 || Scelta> 3);

    if(Scelta == 0) //Se inserisco 0 esco dalla ricerca
    {
        ClearS();
        return;
    }

    getchar(); //Getc per reset degli in
    ClearS(); //Cs

    printf("\n    Inserisci la stringa da cercare\n\n\033[32;1m    Inserimento: \033[0m"); //Chiedo la stringa e la pulisco da ritorni a capo
    fgets(Parametro, sizeof(Parametro), stdin);
    for(i = 0; i< strlen(Parametro); i++)
    {
        if(Parametro[i] == '\n' || Parametro[i] == '\r')
        {
            Parametro[i] = 0;
        }
    }

    ClearS(); //Cs
    i = 1;
    printf("\033[32;1m\n     Num                        Nome                    Cognome                     Numero  \n\033[0m"); // Stampa indicazioni
    while(p!=NULL) //Passo la lista e stampo i risultati
    {
        if(Scelta == 1 && stricmp(p->Nome, Parametro) == 0)
        {
            printf("\n    %3d -> %25s  %25s  %25s", i, p->Nome,p->Cognome,p->Numero); //Considero nomi, cognomi e numeri di lunghezza massima 30 dirante la stampa, in caso di eccesso la stampa viene spostata
            printf("\033[32;1m\n     -------------------------------------------------------------------------------------\033[0m"); //Separatore
            trovato = 1; //Se trovo
        }
        else if(Scelta == 2 && stricmp(p->Cognome, Parametro) == 0)
        {
            printf("\n    %3d -> %25s  %25s  %25s", i, p->Nome,p->Cognome,p->Numero); //Considero nomi, cognomi e numeri di lunghezza massima 30 dirante la stampa, in caso di eccesso la stampa viene spostata
            printf("\033[32;1m\n     -------------------------------------------------------------------------------------\033[0m"); //Separatore
            trovato = 1; //Se trovo
        }
        else if(Scelta == 3 && stricmp(p->Numero, Parametro) == 0)
        {
            printf("\n    %3d -> %25s  %25s  %25s", i, p->Nome,p->Cognome,p->Numero); //Considero nomi, cognomi e numeri di lunghezza massima 30 dirante la stampa, in caso di eccesso la stampa viene spostata
            printf("\033[32;1m\n     -------------------------------------------------------------------------------------\033[0m"); //Separatore
            trovato = 1; //Se trovo
        }
        p = p->next;
        i++;
    }
    if(trovato != 1) //Se non trovo: messaggio d'errore
    {
        printf("\n\n    Stringa non trovata!   \n");
    }

    printf("\033[31;1m\n\n    Premi un tasto qualsiasi per continuare!\n\033[0m"); //Termino
    getchar();
    return;
}
/// /////////////////////////////////////////////////////////////////////////////////
void modifica(s_Nodo *p)
{
    //Variabili
    int i = 1;
    int Scelta = 0;
    int numContatto = 0;
    char Parametro[70];
    int modificato = 0;

    ClearS(); //Cs
    do
    {
        printf("\n\n    Quale contatto vuoi modificare (indicare la posizione del contatto)\n    Inserisci 0 per uscire\n\033[32;1m\n    Inserimento: \033[0m");
        scanf("%d", &numContatto);
        if(numContatto < 0 || numContatto > NumeroDiContatti)
        {
            ClearS();
            printf("\033[31;1m\n    Contatto non esistente!!!   \n\033[0m");
        }
    }
    while(numContatto < 0 || numContatto > NumeroDiContatti);

    if(numContatto == 0)
    {
        ClearS(); //Cs
        return;
    }

    ClearS(); //Cs
    do //Chiedo in che campo modificare
    {
        printf("\n\n    Stai modificando il contatto numero: %3d", numContatto);
        printf("\n\n    Inserisci 1 per modificare il nome\n    Inserisci 2 per modificare il cognome\n");
        printf("    Inserisci 3 per modificare il numero\n    Inserisci 0 per uscire\n\033[32;1m\n    Inserimento: \033[0m");
        scanf("%d", &Scelta);
        if(Scelta<0 || Scelta> 3)
        {
            ClearS();
            printf("\033[31;1m\n    Valore non valido!!!   \n\033[0m");
        }
    }
    while(Scelta<0 || Scelta> 3);

    if(Scelta == 0) //Se inserisco 0 esco dalla modifica
    {
        ClearS();
        return;
    }

    getchar(); //Getc per reset degli in
    ClearS(); //Cs
    printf("\n\n    Inserisci la stringa da modificare\n\033[32;1m\n    Inserimento: \033[0m"); //Chiedo la stringa e la pulisco da ritorni a capo
    fgets(Parametro, sizeof(Parametro), stdin);
    for(i = 0; i< strlen(Parametro); i++)
    {
        if(Parametro[i] == '\n' || Parametro[i] == '\r')
        {
            Parametro[i] = 0;
        }
    }


    ClearS(); //Cs
    i = 0;
    while(p!=NULL && i < NumeroDiContatti) //Passo la lista e stampo i risultati
    {
        i++;
        if(i == numContatto)
        {
            if(Scelta == 1)
            {
                strcpy(p->Nome, Parametro);
            }
            else if(Scelta == 2)
            {
                strcpy(p->Cognome, Parametro);
            }
            else if(Scelta == 3)
            {
                strcpy(p->Numero, Parametro);
            }
            else
            {
                printf("\033[31;1m    Errore interno!  \n\033[0m");
            }
            modificato = 1; //Se trovo
            break;
        }
        p = p->next;
    }

    if(modificato != 1) //Se non trovo: messaggio d'errore
    {
        printf("\033[32;1m\n    Contatto non modificato!   \n\033[0m");
    }

    return;
}
/// /////////////////////////////////////////////////////////////////////////////////
s_Nodo* elimina(s_Nodo *p)
{
    //Variabili
    int i = 1;
    int numContatto = 0;
    s_Nodo *Precedente = p;
    s_Nodo *Primo = p;

    ClearS(); //Cs
    do
    {
        printf("\n\n    Quale contatto vuoi eliminare (indicare la posizione del contatto)\n    Inserisci 0 per uscire\n\033[32;1m\n    Inserimento: \033[0m");
        scanf("%d", &numContatto);//Chiedo il numero del contatto e controllo che sia corretto
        if(numContatto < 0 || numContatto > NumeroDiContatti)
        {
            ClearS();
            printf("\033[31;1m\n    Contatto non esistente!!!   \n\033[0m");
        }
    }
    while(numContatto < 0 || numContatto > NumeroDiContatti);

    if(numContatto == 0) //Se il numero di contatto � 0 esco
    {
        ClearS(); //Cs
        return;
    }

    getchar(); //Getc per reset degli in
    ClearS(); //Cs

    if(numContatto == 1) //Se � il primo returno il secondo
    {
        return p->next;
    }
    //Altrimento mi sposto per avere p in this e precedente in this-1
    p = p->next;
    i = 2;//Inc di i

    while(p!=NULL && i < numContatto) //Passo la lista
    {
        i++; //Incremento i
        Precedente = p; //Incremento p e precendente
        p = p->next;
    }
    Precedente->next = p->next; //Faccio il ponte tra il precedente ed il successivo

    return Primo; //Ritorno il primo
}
/// /////////////////////////////////////////////////////////////////////////////////
void info()
{
    ClearS();
    printf("\033[37;1m\n    Autore: Tiberio Filippo   \033[0m");
    printf("\033[37;1m\n    Classe: 3Ai   \033[0m");
    printf("\033[37;1m\n    Programma: \"Una semplice Rubrica Telefonica in C sfruttando le liste e file\"   \033[0m");
    printf("\033[37;1m\n    Repository: https://gitlab.com/FilippoTi/rubricac   \n\033[0m");

    printf("\033[31;1m\n    Lista Return:   \033[0m");
    printf("\033[37;1m\n        +0 -> tutto corretto!   \033[0m");
    printf("\033[37;1m\n        -7 -> errore nell'aprire il file   \033[0m");
    printf("\033[37;1m\n        -11-> errore nello scrivere il file   \n\033[0m");

    printf("\033[31;1m\n\n    Premi un tasto qualsiasi per continuare!\n\033[0m"); //Termino
    getchar();
    getchar();
    return;
}
/// /////////////////////////////////////////////////////////////////////////////////
